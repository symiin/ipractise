# rm(list=ls())

library(officer)
library(shiny)
library(shinyjs)
library(collapsibleTree)
library(data.tree)
library(shinyWidgets)
library(boxr)
library(stringr)
library(rlist)
library(miscTools)
library(fmsb)
options(shiny.maxRequestSize=15*1024^2) 

server <- function(input, output, session) {
  observeEvent(input$toggleSidebar, {
    shinyjs::toggle(id = "Sidebar")
  })
  tier <<- 1
  name.tier <<- "A"
  all.items <- reactiveValues(List = list(), name.List = list(), network.DF = data.frame())
  output$name <- renderUI({
    
    titlePanel(input$name)
  })
  suffix <- function(a){
    validate(
      need(is.numeric(input$first),'')
    )
    if(a==1){
      return("st")
    }else if(a==2){
      return("nd")
    }else if(a==3){
      return("rd")
    }else{
      return("th")
    }
  }
  
  
  
  
  #### Load existing rds file ####
  
  output$load <- renderUI({
    
    # filelist_pub <<- as.data.frame(box_search(".rds", ancestor_folder_ids = 82900382159))
    Courses = list.files(path = "./ArchivedRDS/")
    filez = tools::file_path_sans_ext(Courses)
    filelist_pub <<- as.data.frame(Courses)
    tagList(
      #h4("Load course structures and user profile information from Box.com:"),
      br(),
      # textInput(inputId = "Username_pub", label = "Please enter your user name here"),
      selectInput(inputId = "loadId_box_pub", label="Please select the course of interest:",
                  choices = filez))
  })
  # User survey results
  observeEvent(input$loadId_box_pub,priority = 1,{
    # filelist_user <<- as.data.frame(box_search(".csv", ancestor_folder_ids = 94119353943))
    filelist_user <<- as.data.frame(list.files(path = "./user_profile/"))
    logfile <<- read.csv("logfile.csv")
    logfile <- logfile %>% filter(role == "Student")
    lastrow <- nrow(logfile)
    logfile2 <<- logfile[lastrow-1,3]
    logfile2pcsv <<- paste0(logfile2, '.csv', sep = '')
    id_user <<- which(filelist_user == "gygy.csv")
    # id_user <<- which(filelist_user == paste0(input$Username_pub,'.csv'))
    # id_user <<- as.numeric(filelist_user[id_user, "id"])
    # user1<<-box_read_csv(file_id=id_user) 
    user1 <<- read.csv(paste0('./user_profile/', filelist_user[id_user,], sep = ''), header = T)
    # user1 <<- user1[nrow(user1),]
    user2 <<- user1[nrow(user1), ]
    #programming experience no=1, min=2, prior=3, beginer to interm=4, interm to adv = 5, expert = 6
    #Learning method: ideo to watch =1, stuff to read = 2, app to play = 3, try as you read =4
    # Diff
    
      if (user2[,2]=='Much easier'){
        user2[,2] = 1}else if(user2[,2] == 'Kind of easier'){
        user2[,2] = 2}else if(user2[,2] == 'Stay at current level'){
        user2[,2] = 3}else if(user2[,2] == 'More challenging'){
        user2[,2] = 4}else if(user2[,2] == 'Super challenging'){
        user2[,2] = 5
      }
    # Method
      if (user2[,6] == "Stuff to Read"){
        user2[,6] = 1}else if(user2[,6] == "Try as you read"){
        user2[,6] = 2}else if(user2[,6] == "No preference"){
        user2[,6] = 3}else if(user2[,6] == "App to play"){
        user2[,6] = 4}else if(user2[,6] == "Video to watch"){
        user2[,6] = 5
      }
    # for (i in 1:nrow(user2)){
    #   if (user2$`Which learning method do you prefer the most?`[i]=="Stuff to Read"){
    #     user2[user2$`Which learning method do you prefer the most?`[i]=="Stuff to Read",]$`Which learning method do you prefer the most?`[i]= 1 # "Passive Viewing"
    #   }else if(user2$`Which learning method do you prefer the most?`[i]=="Read and try on your own"){
    #     user2[user2$`Which learning method do you prefer the most?`[i]=="Read and try on your own",]$`Which learning method do you prefer the most?`[i]= "Read and try on your own"
    #   }else if (user2$`Which learning method do you prefer the most?`[i]== "Hands-on practice"){
    #     user2[user2$`Which learning method do you prefer the most?`[i]== "Hands-on practice",]$`Which learning method do you prefer the most?`[i]= "Hands-on Practice"
    #   }else if (user1$`Which learning method do you prefer the most?`[i]=="Video to watch"){
    #     user2[user2$`Which learning method do you prefer the most?`[i]=="Video to watch",]$`Which learning method do you prefer the most?`[i]= "Video to watch"
    #   }
    # }
    
    # Exp
      if (user2[,3] == "None"){
        user2[,3] = 1}else if(user2[,3] == 'a little'){
        user2[,3] = 2}else if(user2[,3] == 'less than Neutral'){
        user2[,3] = 3}else if(user2[,3] == 'more than Neutral'){
        user2[,3] = 4}else if(user2[,3] == 'Neutral'){
        user2[,3] = 5
      }
    # Time
    #for (i in 1:nrow(user2)) {
      if (user2[,4] == 'less than 15 mins'){
        user2[,4] = 1}else if(user2[,4] == '15-30 mins'){
        user2[,4] = 2}else if(user2[,4] == '30-45 mins'){
        user2[,4] = 3}else if(user2[,4] == '45-60 mins'){
        user2[,4] = 4}else if(user2[,4] == 'more than 60 mins'){
        user2[,4] = 5
      }
    #}
    # for (i in 1:nrow(user1)){
    #   if (user1$`Which learning method do you prefer the most?`[i]=="Passive Viewing"){
    #     user1[user1$`Which learning method do you prefer the most?`[i]=="Passive Viewing",]$`Which learning method do you prefer the most?`[i]=1
    #   }else if(user1$`Which learning method do you prefer the most?`[i]=="Read and try on your own"){
    #     user1[user1$`Which learning method do you prefer the most?`[i]=="Read and try on your own",]$`Which learning method do you prefer the most?`[i]=2
    #   }else if (user1$`Which learning method do you prefer the most?`[i]=="Hands-on practice"){
    #     user1[user1$`Which learning method do you prefer the most?`[i]=="Hands-on practice",]$`Which learning method do you prefer the most?`[i]=3
    #   }else if (user1$`Which learning method do you prefer the most?`[i]=="Video to watch"){
    #     user1[user1$`Which learning method do you prefer the most?`[i]=="Video to watch",]$`Which learning method do you prefer the most?`[i]=4
    #   }
    # }
    up <<- data.frame(difficulty = user2[,2], time = user2[,4], experience = user2[,3], method = user2[,6])
    up$difficulty = as.numeric(up$difficulty)
    up$time = as.numeric(up$time)
    up$experience = as.numeric(up$experience)
    up$method = as.numeric(up$method)
  })
  
  
  new <- reactiveVal("YES")
  
  #### Materials printed when clicked ####   
  inputs <- reactiveVal(c())
  observeEvent(input$click, {
    validate(need(!is.null(tryCatch(loaded_user_data()$phase1.2=="NO", error=function(e) NULL)), label=''))
    materials_ <- materials$new
    validate(need(tryCatch(input$click[[length(input$click)]], error=function(e)all.items$network.DF[1,1]) %in% names(materials_), label=""))
    index.url <<- (which(materials_[[input$click[[length(input$click)]]]][-1,2]=="url"))
    index.file <<- (which(materials_[[input$click[[length(input$click)]]]][-1,2]!="url"))
    
    if(length(index.file)!=0){
      compare <- data.frame(difficulty = materials_[[ input$click[[length(input$click)]] ]][c(index.file+1),3],
                            time = materials_[[ input$click[[length(input$click)]] ]][c(index.file+1),5],
                            experience = materials_[[ input$click[[length(input$click)]] ]][c(index.file+1),6],
                            method = materials_[[ input$click[[length(input$click)]] ]][c(index.file+1),7])
      compare$method = as.character(compare$method)
      compare$experience = as.character(compare$experience)
      compare$difficulty = as.numeric(compare$difficulty)
      compare$time = as.numeric(compare$time)
      
      compare$method = tolower(compare$method)
      compare$experience = tolower(compare$experience)
      # Change Experience to integer
      for (i in 1:nrow(compare)){
        if (compare$experience[i] =="none" | compare$experience[i] =="no"){
          compare[compare$experience[i] =="none" | compare$experience[i]=="no",]$experience[i]=1
        }else if (compare$experience[i] =="minimum" | compare$experience[i] =="a little"){
          compare[compare$experience[i] =="minimum" | compare$experience[i] =="a little",]$experience[i]=2
        }else if (compare$experience[i] =="prior programming helpful" | compare$experience[i] =="some"){
          compare[compare$experience[i] =="prior programming helpful" | compare$experience[i] =="some",]$experience[i]=3
        }else if (compare$experience[i] =="beginner to intermediate" | compare$experience[i] =="quite a lot"){
          compare[compare$experience[i] =="beginner to intermediate" | compare$experience[i] =="quite a lot",]$experience[i]=4
        }else if (compare$experience[i] =="more than neutral" | compare$experience[i] =="professional"){
          compare[compare$experience[i] =="more than neutral" | compare$experience[i] =="professional",]$experience[i]=5
        }else if (compare$experience[i] =="na" | compare$experience[i] =="na"){
          compare[compare$experience[i] =="na" | compare$experience[i] =="na",]$experience[i]=NA
        }
      }
      # Change Time to integer
      for (i in 1:nrow(compare)){
        if (compare$time[i] >=0 & compare$time[i] <= 15){
          compare[compare$time[i] > 0 & compare$time[i] <= 15,]$time[i]=1
        }else if (compare$time[i] > 15 & compare$time[i] <= 30){
          compare[compare$time[i] > 15 & compare$time[i] <= 30,]$time[i]=2
        }else if (compare$time[i] > 30 & compare$time[i] <= 45){
          compare[compare$time[i] > 30 & compare$time[i] <= 45,]$time[i]=3
        }else if (compare$time[i] > 45 & compare$time[i] <= 60){
          compare[compare$time[i] > 45 & compare$time[i] <= 60,]$time[i]=4
        }else if (compare$time[i] > 60){
          compare[compare$time[i] > 60,]$time[i]=5
        }else if (compare$time[i] == 999){
          compare[compare$time[i] == 999,]$time[i]=999
        }
      }
      #change the Method to integer
      for (i in 1:nrow(compare)){
        if (compare$method[i]=="passive viewing" | compare$method[i]=="stuff to read"){
          compare[compare$method[i]=="passive viewing" | compare$method[i]=="stuff to read",]$method[i]=1
        }else if(compare$method[i]=="read and try on your own" | compare$method[i]=="try as you read"){
          compare[compare$method[i]=="read and try on your own" | compare$method[i]=="try as you read",]$method[i]=2
        }else if(compare$method[i]=="no preference"){
          compare[compare$method[i]=="no preference",]$method[i]=3
        }else if (compare$method[i]=="hands-on practice" | compare$method[i]=="app to play"){
          compare[compare$method[i]=="hands-on practice",]$method[i]=4
        }else if (compare$method[i]=="video to watch" | compare$method[i]=="video to watch"){
          compare[compare$method[i]=="video to watch" | compare$method[i]=="video to watch",]$method[i]=5
        }else if (compare$method[i]=="na" | compare$method[i]=="na"){
          compare[compare$method[i]=="na" | compare$method[i]=="na",]$method[i]=NA
        }
      }
      
      
      # #the whole matrix (Don't need for now - TJ)
      whole <<-rbind(up, compare)
      dis_whole <<- dist(whole)
      #diff
      whole_diff <<- whole$difficulty
      dis_diff <<- dist(whole_diff)
      #time
      whole_time <<- whole$time
      dis_time <<- dist(whole_time)
      #experience
      whole_exp <<- whole$experience
      dis_exp <<- dist(whole_exp)
      #method
      whole_med <<- whole$method
      dis_med <<- dist(whole_med)
      
      print(dis_whole[c(1:nrow(compare))])
      which_list = sort(dis_whole[c(1:nrow(compare))])
      num_which_list <- length(which_list)
      wihch_array = numeric(num_which_list)
      for (i in 1:num_which_list){
        wihch_array[i] = which(dis_whole == which_list[i], arr.ind = TRUE)
      }
      # distance_matrix = dis_whole[which(materials1[[node.name]][,2] == input$viewid)-1]
      # similarity = round((1/(1+distance_matrix))*100,digits = 2)
      # percentage = round((similarity/3.17)*100,digits = 2)
      
      # choices2 <<- c("Select Resources",materials_[[ input$click[[length(input$click)]] ]][-1,2][wihch_array])
      # names(choices2) <<- c( "Select Resources",materials_[[ input$click[[length(input$click)]] ]][c(index.file+1),2][wihch_array])
      
      choices2 <<- c("Select Resources",materials_[[ input$click[[length(input$click)]] ]][-1,2])
      names(choices2) <<- c( "Select Resources",materials_[[ input$click[[length(input$click)]] ]][c(index.file+1),2])
      
      
      
    }
    
    
    output$materials.view.files.buttons <- renderUI({
      if(length(index.file)!=0){
        tagList(
          selectInput(inputId = "viewid", label = paste0("Here are the materials that we recommend to you ordered from the most fitted to least fitted for the topic ", node.name, ":"),
                      choices = choices2,
                      selected = NULL,
                      multiple = F)
          #choices = choices2,
          #selected = NULL,
          #multiple = F),
          # conditionalPanel("input.viewid!=''",
          #                  actionButton("fileview",label="view")),
          # br()
        )
      }
      else{
      }
    })
  })
  materials <- reactiveValues(new = list())
  terminal <- reactiveValues(new = c())
  
  
  #### load R object from Box personl and public####
  loaded_user_data <- reactiveVal(list())
  
  observeEvent(input$loadId_box_pub, priority = 1, {
    course_selected <<- paste0(input$loadId_box_pub, '.rds')
    output$show <- renderUI({
      # tagList(
      #   h4(sprintf("User Name: %s", input$Username_pub)),
      #   img(src = "https://www.stickpng.com/assets/images/5a461402d099a2ad03f9c997.png", height = 25, width = 25),
      #   h5(sprintf("Difficulty: %s ", 
      #              user2$`How do you prefer your difficulty level?`)),
      #   img(src = "https://ak6.picdn.net/shutterstock/videos/31034746/thumb/1.jpg", height = 25, width = 25),
      #   h5(sprintf("Accepted Max Time (mins): %s", 
      #              user2$`What is the maximum training time for one particular traing material that you would accept?`)),
      #   img(src = "https://webcomicms.net/sites/default/files/clipart/150339/computer-cartoon-images-150339-6054192.jpg", height = 25, width = 25),
      #   h5(sprintf("Programming Experience: %s",
      #              user2$`How much programming experience do you think you have before training with this material?`)),
      #   img(src = "https://as1.ftcdn.net/jpg/01/95/35/78/500_F_195357805_his1UjQcJqJiJohgiYnK5cwdVu8G5Ldd.jpg", height = 25, width = 25),
      #   h5(sprintf("Method: %s",
      #              user2$`Which learning method do you prefer the most?`))
      # )
      # tagList(
      #   h4(div(sprintf("%s 's Training Peferences", input$Username_pub)),color = "d3d3d3"),
      #   #img(src = "https://www.stickpng.com/assets/images/5a461402d099a2ad03f9c997.png", height = 25, width = 25),
      #   h5(div(icon("file-alt", class = NULL, lib = "font-awesome"), sprintf("Skill Level (Ability): %s ", "Novice"))),
      #   #user2$`How do you prefer your difficulty level?`))),
      #   #img(src = "https://ak6.picdn.net/shutterstock/videos/31034746/thumb/1.jpg", height = 25, width = 25),
      #   h5(div(icon("clock", class = NULL, lib = "font-awesome"), sprintf("Preferred Max Time (mins): %s",
      #                  user2$`What is the maximum training time for one particular traing material that you would accept?`))),
      #   #img(src = "https://webcomicms.net/sites/default/files/clipart/150339/computer-cartoon-images-150339-6054192.jpg", height = 25, width = 25),
      #   h5(div(icon("keyboard", class = NULL, lib = "font-awesome"), sprintf("Coding Experience: %s",
      #                  user2$`How much programming experience do you think you have before training with this material?`))),
      #   h5(div(icon("lightbulb", class = NULL, lib = "font-awesome"), sprintf("Method: %s",user2$`Which learning method do you prefer the most?`)))
        
      # )
      
    })
    
    # ids <- as.data.frame(box_search(".rds", ancestor_folder_ids = 82900382159))
    # ids <- as.data.frame(box_search(".rds", ancestor_folder_ids = 82900382159))
    ids <<- as.data.frame(list.files(path = "./ArchivedRDS/"))
    # if(sum(ids$name %in% input$loadId_box_pub)==1){
      if(any(ids == paste0(input$loadId_box_pub, '.rds'))==1){
      output$warning1 <- renderText({
        NULL
      })
      # id <- which(ids$name == input$loadId_box_pub)
      id <- which(ids == paste0(input$loadId_box_pub, '.rds'))
      # id <- as.numeric(ids[id, "id"])
    }else{
      output$warning1 <- renderText({
        "Please enter a valid existing course structure"
      })
      warning("UH OH")
      validate(need(F, ""))
    }
    tryCatch(
      # loaded_user_data(box_read(file_id = id)),
      loaded_user_data(readRDS(paste0('./ArchivedRDS/', ids[id,], sep = ''))),
      error=function(e) print("remember to run box_auth in console!")
    )
    temp <- loaded_user_data()
    nams = length(temp[[8]])
    for(i in 1:nams){
      ztemp = temp[[8]][i]
      if(nrow(ztemp[[1]]) == 1 || is.null(dim(ztemp[[1]])) == TRUE){
        ztemp[[1]] = rbind(ztemp[[1]], c("None", "None", "NA", "There are Currently No Materials in This Section.", 999, "NA", "NA"))
        print(ztemp[[1]])
        temp[[8]][i] = ztemp}else{print('')}
    }
    # names(temp) <- c("List", "name.List", "phase1.1", "phase1.2", "tier", "name.tier", "tree", "materials", "terminal")
    names(temp) <- c("List", "name.List", "phase1.1", "phase1.2", "tier", "name.tier", "tree", "materials", "terminal", "Next", "more", "which")
    loaded_user_data(temp)
    loaded <<- loaded_user_data()
    all.items$List <- loaded_user_data()$List
    all.items$name.List <- loaded_user_data()$name.List
    all.items$network.DF <- loaded_user_data()$tree
    output$name <- renderUI({
      titlePanel(all.items$network.DF[,1])
    })
    
    if(loaded_user_data()$phase1.1 == "NO"){
      warning("click done")
      click("done")
    }
    
    newNode <<- FromDataFrameNetwork(all.items$network.DF)
    tier <<- loaded_user_data()$tier
    name.tier <<- loaded_user_data()$name.tier
    materials$new <- loaded_user_data()$materials #already named
    terminal$new <- loaded_user_data()$terminal #need to print materials
  })
  
  
  
  #### Tree Plot ####
  observeEvent(input$loadId_box_pub, label = "PLOT", priority = -2, {
    # output$plot <- renderCollapsibleTree({
    #   framework = FromDataFrameNetwork(all.items$network.DF)
    #   temp_num = dim(framework)[1]
    #   #framework$Color = framework[,2]
    #   #levels(framework$Color) <- colorspace::rainbow_hcl(temp_num)
    #   collapsibleTree(framework,fill = "#00cccc",
    #                   nodeSize = "leafCount",collapsed = FALSE, inputId = "click")
    # })
    output$plot <- renderCollapsibleTree({
      collapsibleTree(FromDataFrameNetwork(all.items$network.DF), fill = "#E495A5",collapsed = F, inputId = "click")
    })
    
  })
  observeEvent(input$click,priority = 1,{
    node.name <<- tryCatch(input$click[[length(input$click)]], error=function(e)all.items$network.DF[1,1])
  })
  
  ####load website link material ####
  observeEvent(input$viewid, priority = 5, {
    output$websitelink <- renderUI({
      materials1 <<- materials$new
      
      # filelist_user <- as.data.frame(box_search(".csv", ancestor_folder_ids = 94119353943))
      # id_user <- which(filelist_user$name == 'gygy.csv')
      # id_user <- as.numeric(filelist_user[id_user, "id"])
      # user1<-box_read_csv(file_id=id_user)
      # up <- data.frame(difficulty = user1[,2], time = user1[,4], experience = user1[,3], method = user1[,8])
      # up$difficulty = as.numeric(up$difficulty)
      # up$time = as.numeric(up$time)
      # up$experience = as.numeric(up$experience)
      # up$method = as.numeric(up$method)
      # 
      # 
      # compare <- data.frame(difficulty = materials1[["Basics"]][-1,3], time = materials1[["Basics"]][-1,5],
      #                       experience = materials1[["Basics"]][-1,6], method = materials1[["Basics"]][-1,7])
      # compare$method = as.character(compare$method)
      # compare$experience = as.character(compare$experience)
      # #change the programming experience to integer
      # for (i in 1:nrow(compare)){
      #   if (compare$experience[i] =="No"){
      #     compare[compare$experience[i] =="No",]$experience[i]= 1
      #   }else if(compare$experience[i] =="Minimum"){
      #     compare[compare$experience[i] =="Minimum",]$experience[i]=2
      #   }else if (compare$experience[i] =="Prior programming helpful"){
      #     compare[compare$experience[i] =="Prior programming helpful",]$experience[i]=3
      #   }else if (compare$experience[i] =="Beginner to Intermediate"){
      #     compare[compare$experience[i] =="Beginner to Intermediate",]$experience[i]=4
      #   }
      # }
      # #change the Method to integer
      # for (i in 1:nrow(compare)){
      #   if (compare$method[i] =="Passive viewing"){
      #     compare[compare$method[i]=="Passive viewing",]$method[i]=1
      #   }else if(compare$method[i]=="Read and try on your own"){
      #     compare[compare$method[i]=="Read and try on your own",]$method[i]=2
      #   }else if (compare$method[i]=="Hands-on Practice"){
      #     compare[compare$method[i]=="Hands-on Practice",]$method[i]=3
      #   }else if (compare$method[i]=="Video to watch"){
      #     compare[compare$method[i]=="Video to watch",]$method[i]=4
      #   }
      # }
      # 
      # #calsulate the distance as a whole
      # whole <-rbind(up, compare)
      
      
      # # ORIGINAL YANG CODE 
      # dis_whole <- dist(whole)
      
      
      # #diff
      # whole_diff <<- whole$difficulty
      # dis_diff <<- dist(whole_diff)
      # #time
      # whole_time <<- whole$time
      # dis_time <<- dist(whole_time)
      # #experience
      # whole_exp <<- whole$experience
      # dis_exp <<- dist(whole_exp)
      # #method
      # whole_med <<- whole$method
      # dis_med <<- dist(whole_med)
      # 
      # which_list = sort(dis_whole[c(1:nrow(compare))])
      # num_which_list <-length(which_list)
      # wihch_array=numeric(num_which_list)
      # for (i in 1:num_which_list){
      #   wihch_array[i]= which(dis_whole == which_list[i], arr.ind = TRUE)
      # }
      # 
      # choice_hhhhh = materials1[["Basics"]][-1,2][wihch_array]
      # materials1[["Basics"]][-1,4][3]
      # dis_whole[wihch_array]
      # which(materials1[["Basics"]][,2] == "Introductory Base R operations")
      # dis_whole[which(materials1[["Basics"]][,2] == "Introductory Base R operations")-1]
      # 
      
      # #OLD whole percentage
      # distance_matrix = dis_whole[which(materials1[[node.name]][,2] == input$viewid)-1]
      # similarity = round((1/(1+distance_matrix))*100,digits = 2)
      # percentage = round((similarity/100)*100,digits = 2)
      # # percentage = round((similarity/3.17)*100,digits = 2)
      
      #diff percentage
      distance_matrix_diff = dis_diff[which(materials1[[node.name]][,2] == input$viewid)-1]
      similarity_diff = round((1/(1+distance_matrix_diff))*100, digits = 2)
      percentage_diff = round((similarity_diff/100)*100, digits = 2)
      # percentage_diff = round((similarity_diff/31.5)*100,digits = 2)
      
      #time
      distance_matrix_time = dis_time[which(materials1[[node.name]][,2] == input$viewid)-1]
      similarity_time = round((1/(1+distance_matrix_time))*100,digits = 2)
      percentage_time = round((similarity_time/100)*100,digits = 2)
      # percentage_time = round((similarity_time/3.5)*100,digits = 2)
      
      #exp
      distance_matrix_exp= dis_exp[which(materials1[[node.name]][,2] == input$viewid)-1]
      similarity_exp = round((1/(1+distance_matrix_exp))*100,digits = 2)
      percentage_exp = round((similarity_exp/100)*100,digits = 2)
      # percentage_exp = round((similarity_exp/33.4)*100,digits = 2)
      
      #med
      distance_matrix_med= dis_med[which(materials1[[node.name]][,2] == input$viewid)-1]
      similarity_med = round((1/(1+distance_matrix_med))*100,digits = 2)
      percentage_med = round((similarity_med/100)*100,digits = 2)
      
      #New whole percentage equation - TJ
      percentage = (percentage_diff + percentage_exp + percentage_med + percentage_time)/4
      
      #combine together to be a dataframe
      #data = data.frame(percentage,percentage_diff,percentage_time,percentage_exp,percentage_med)
      data = data.frame(80,56,78,90,13)
      colnames(data) = c("Whole" , "Difficulty" , "Estimated Time" , "Coding Experience" , "Method")
      data <- rbind(rep(100,5) , rep(0,5) , data)
      colors_border = rgb(0.2, 0.5, 0.5, 0.9)
      
      colors_in = rgb(0.2, 0.5, 0.5, 0.4)
      
      #materials1[["Basics"]][2,4]
      #dis_whole[2]
      comp = c("None", "None", "NA", "There are Currently No Materials in This Section.", 999, "NA", "NA")
      if(!input$viewid == "Select Resources"){
        # St4t414fall2o2o
        if(materials1[[node.name]][which(materials1[[node.name]][,2] == input$viewid),] != comp){
        tagList(
          #h5(paste0(sprintf("Similarity Percentage based on your survey profile: %s", percentage),"%")),
          #progressBar(id = "bar_value", value = percentage, size = 'sm', status = 'red',unit_mark = "%"),
          br(),
          tags$a(href=materials1[[node.name]][which(materials1[[node.name]][,2] == input$viewid)],target="_blank", "See the website here"),
          br(),
          sprintf("Short Description: %s",
                  materials1[[node.name]][which(materials1[[node.name]][,2] == input$viewid),2]),
          br(),
          paste0(sprintf("Difficulty: %s",
                         materials1[[node.name]][which(materials1[[node.name]][,2] == input$viewid),3])),
          br(),
          sprintf("Detailed Description: %s",
                  materials1[[node.name]][which(materials1[[node.name]][,2] == input$viewid),4]),
          br(),
          paste0(sprintf("Estimated Time (mins): %s",
                         materials1[[node.name]][which(materials1[[node.name]][,2] == input$viewid),5],percentage_time)),
          br(),
          paste0(sprintf("Coding Experience: %s",
                         materials1[[node.name]][which(materials1[[node.name]][,2] == input$viewid),6], percentage_exp)),
          br(),
          paste0(sprintf("Method: %s",
                         materials1[[node.name]][which(materials1[[node.name]][,2] == input$viewid),7], percentage_med)),
          br(),
          br(),
          br(),
          h5(paste0(sprintf("Similarity Percentage based on your survey profile: %s", percentage),"%")),
          progressBar(id = "bar_value", value = percentage, size = 'sm', status = 'red',unit_mark = "%")
        )
      }else if(materials1[[node.name]][which(materials1[[node.name]] == input$viewid)] == comp){tagList(
          br(),
          tags$a(href=materials1[[node.name]][which(materials1[[node.name]][,2] == input$viewid)],target="_blank", "See the website here"),
          br(),
          sprintf("Short Description: %s",
                  materials1[[node.name]][which(materials1[[node.name]][,2] == input$viewid),2]),
          br(),
          paste0(sprintf("Difficulty: NA")),
          br(),
          sprintf("Detailed Description: There are no materials for this node currently"),
          br(),
          paste0(sprintf("Estimated Time (mins): NA")),
          br(),
          paste0(sprintf("Coding Experience: NA")),
          br(),
          paste0(sprintf("Method: NA")),
          br(),
          br(),
          br()
      )
      }
      
      }
    })
    
    #### Generate Radar Plot (need improvement) ####
    output$radarPlot <- renderPlot({
      #OLD whole percentage
      # distance_matrix = dis_whole[which(materials1[[node.name]][,2] == input$viewid)-1]
      # similarity = round((1/(1+distance_matrix))*100,digits = 2)
      # percentage <<- round((similarity/100)*100,digits = 2)
      # # percentage <<- round((similarity/3.17)*100,digits = 2)
      
      #diff percentage
      distance_matrix_diff = dis_diff[which(materials1[[node.name]][,2] == input$viewid)-1]
      similarity_diff = round((1/(1+distance_matrix_diff))*100,digits = 2)
      percentage_diff <<- round((similarity_diff/100)*100,digits = 2)
      # percentage_diff <<- round((similarity_diff/31.5)*100,digits = 2)
      
      #time
      distance_matrix_time = dis_time[which(materials1[[node.name]][,2] == input$viewid)-1]
      similarity_time = round((1/(1+distance_matrix_time))*100,digits = 2)
      percentage_time <<- round((similarity_time/100)*100,digits = 2)
      # percentage_time <<- round((similarity_time/3.5)*100,digits = 2)
      
      #exp
      distance_matrix_exp= dis_exp[which(materials1[[node.name]][,2] == input$viewid)-1]
      similarity_exp = round((1/(1+distance_matrix_exp))*100,digits = 2)
      percentage_exp <<- round((similarity_exp/100)*100,digits = 2)
      # percentage_exp <<- round((similarity_exp/33.4)*100,digits = 2)
      
      #med
      distance_matrix_med= dis_med[which(materials1[[node.name]][,2] == input$viewid)-1]
      similarity_med = round((1/(1+distance_matrix_med))*100,digits = 2)
      percentage_med <<- round((similarity_med/100)*100,digits = 2)
      
      # New Whole Real Equation - TJ
      percentage <<- (percentage_diff + percentage_exp + percentage_med + percentage_time)/4
      
      ###combine together to be a data frame###
      data = data.frame(percentage,percentage_diff,percentage_time,percentage_exp,percentage_med)
      #data = data.frame(73.19,61.05,89.14,10.24,100)
      colnames(data) = c("Overall Fit" , "Difficulty" , "Estimated Time" , "Coding Experience" , "Method")
      data <- rbind(rep(100,5) , rep(0,5) , data)
      
      colors_in = rgb(0.2, 0.5, 0.5, 0.4)
      if(!input$viewid == "Select Resources"){
        radarchart(
          data  ,
          axistype = 1 ,
          #custom polygon
          pcol = '#00cccc' ,
          pfcol = colors_in ,
          plwd = 4 ,
          plty = 1,
          #custom the grid
          cglcol = "#060E58",
          cglty = 1,
          axislabcol = "#060E58",
          caxislabels = seq(0, 100, 25),
          cglwd = 1.5,
          #custom labels
          vlcex = 1
        )
      }
    })
    
    
  })
  
  
}



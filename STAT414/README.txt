In each folder,

- 'server.R' allows you to experience the mirtCAT test we actually used for data collection. If you want, run the shinyapp using this file

- 'IRT-XXX.Rmd' includes all codes for data processing, simple descriptive analysis, and IRT estimation

- 'IRT-XXX.html' is a rendered html page based on the above Rmd file
- Many '.csv' files include user responses and item information. Any related inquiries can be sent to jzl95@psu.edu


 
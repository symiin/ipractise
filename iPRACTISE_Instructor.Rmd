---
title: "iPRACTISE"
resource_files:

- instructor_view/server.R
- instructor_view/ui.r
- test_generator/server.R
- test_generator/ui.R
- ../stems/visual_001_ggplot_geomPoint_01_files/figure-markdown_strict/scatter-1.png
- ../stems/visual_001_ggplot_geomPoint_01_files/figure-markdown_strict/scatter2-1.png
- ArchivedRDS
- test_boxr

runtime: shiny
output: flexdashboard::flex_dashboard
---
```{r setup, echo=FALSE, message=FALSE}
# rmarkdown::run("iPRACTISE2.Rmd", shiny_args=list(host="66.71.34.79", port = 8888))
knitr::opts_chunk$set(echo = FALSE)
library(mirtCAT)
library(data.tree)
library(collapsibleTree)
library(dplyr)
library(colorspace)
library(boxr)
library(shinyBS)
library(sodium)
library(glue)
library(shinyauthr)
library(shinydashboard)
library(data.table)

# packages for mirtCAT
library(ggplot2)
library(testthat)

```

```{r}
# Directories within test_generator
# itempath <- "./test_generator/database.csv" ## mirtCAT itembank, for the integrated app
# modelpath <- "./test_generator/model.rdata" ## mirtCAT model repository,for the integrated app
# itembank <- read.csv(file = itempath, stringsAsFactors = F) ## For the integrated app
# load(modelpath)
```

```{r design, eval=TRUE}
# The 'design' argument of mirtCAT
# See mirtCAT help page.
# design_list <- list(max_items = 5, min_SEM = 0.9)
```

```{r mirtUI, eval=TRUE}
# Independent UI of mirtCAT
# mirtCAT arguments related to UI
# change aesthetics of GUI, including title, authors, header, and initial message
# title <- "iPRACTISE Computerized Adaptive Test on R"
# authors <- "The iPRACTISE team"
# firstpage <- list(h2("Example Test"),
#                   h5("Please answer each item to the best of your ability."))#,        
#                                          #"The results of this test will remain completely anonymous ",
#                                          #"and are only used for research purposes."))
#                                          
# lastpage <- function(person) {
#   list(h3("Thank you for completing the test."),
#        h3("Please do NOT close the tab until instructed to do so."))
# }
#
# demographics <- list(
#   textInput(inputId = "id", label = "PSU ID", value = ""))
#
# shinyGUI_list <- list(title = title, authors = authors,
#                       demographics = demographics,
#                       demographics_inputIDs = c("id"),
#                       firstpage = firstpage, lastpage = lastpage)
```

```{r server}
## demo for how to get profile id
# read in log file
log <- fread("logfile.csv")
ses <- as.character(session$token)
userls <- log[which(log$session == ses),]      
profile <- userls$profile
## end demo

observeEvent(input$login, {

          appendTab(
                  inputId = "tabs",  
                    tabPanel("Instructor Version",
                            tags$h4("Welcome to the Individualized Pathways and Resources to Adaptive Control Theory-Inspired Scientific Education (iPRACTISE) app!"),
                            textOutput("profile_name"),
                            tags$hr(),
                            tags$br(),
                            tags$h5("Our team believes that digital learning should work like the cruise control of a car. We tell the system how fast we want to go and it provides personalized solutions to help us achieve our goals automatically."),

                            tags$h5("In this beta version of iPRACTISE, you, as an instructor, will go through these steps to create a course structure that you can then use with your own student:"),
                            tags$br(),
                            fluidPage(align = "center",
                            fluidRow(
                              column(width=8, align="center",
                            DiagrammeR::grViz("
                            digraph graph2 {

                            graph [layout = dot, rankdir = LR]

                            # node definitions with substituted label text
                            node [shape = rectangle, fillcolor = Biege]
                            a [label = 'Create a new course \n or browse from our \n archive of existing courses.']
                            b [label = 'Add or edit training \n materials for your course.\n You will be asked to \n specify selected attributes \n of these materials to \n better tailor to what \n students needs.']
                            c [label = 'Use student view \n to review your \n course structure.']
                            d [label = 'Create test to assess \n your students.']

                            a -> b -> c -> d

                            }
                            ")))),

                            tags$br(),
                            tags$h5("Note: Currently only fixed-length tests are supported, but options are available to automate creation of adaptive tests with the R package, Multidimentional Item Response Theory Computerized Adaptive Test (mirtCAT; Chalmers, 2016 PLEASE ADD LINK TO THIS URL: https://cran.r-project.org/web/packages/mirtCAT/index.html) Please contact symiin@psu.edu for details."),
                            tags$br(),
                            tags$h5("Reference:"),
                            tags$h5("R. Philip Chalmers (2016). Generating Adaptive and Non-Adaptive Test Interfaces for Multidimensional Item Response Theory Applications. Journal of Statistical Software, 71(5), 1-39. doi:10.18637/jss.v071.i05"),
                          icon = icon("door-open")
                          )

              )

              # a way to add a shiny app to rmarkdown

              appendTab(
                inputId="tabs",
                tabPanel("Topics",
                  shinyAppDir("./instructor_view",
                              options = list(width = "100%", height = 800)),
                  icon = icon("book")
                  )
                )
              appendTab(
                inputId="tabs",
                tabPanel("Student View",
                  shinyAppDir("./student_view",
                              options = list(width = "100%", height = 800)),
                  icon = icon("user-graduate"))
                )
              
              appendTab(
                inputId="tabs",
              #   tabPanel("Create Test Template",
              #     shinyAppDir("./test_generator",
              #                 options = list(width = "100%", height = 800)),
              #     icon = icon("plus-square"))
              # )
                tabPanel("Create Test Template",
                         tags$h3("Please click the Redirect Button to redirect to our Test Generator App."),
                         actionButton(inputId='redirect', label="Redirect",
                          icon = icon("plus-square"),
                          onclick = "window.open('http://18.218.104.174/shiny/rstudio/test_generator/')")
              ))
            updateTabItems(session, inputId = "tabs", selected = "Instructor Version")
})

output$profile_name<-renderText({
  paste("Welcome Back!",profile)
})

#"window.open('https://rstudio-connect.tlt.psu.edu:3939/connect/#/apps/287/access')") 
```

Given data.frame **midwest**, the code for creating a scatterplot of
**area** (x axis) vs. **poptotal** (y axis) is:

<pre><code>ggplot(midwest, aes(x=area, y=poptotal)) + <br>
  geom_point()
</code></pre>
<img src="visual_002_ggplot_geomPoint_02_files/figure-markdown_strict/scatter-1.png" width="60%" />

You need to differentiate the points by the variable **state** with
different colors.

<img src="visual_002_ggplot_geomPoint_02_files/figure-markdown_strict/unnamed-chunk-1-1.png" width="60%" />

<pre><code>ggplot(midwest, aes(x=area, y=poptotal)) + <br>
  geom_point(aes(<span style="color:red">___</span>=state))
</code></pre>
What would fill in the blank space above? [Click
here](https://ggplot2.tidyverse.org/reference/ "ggplot2") to see the
ggplot2 reference.

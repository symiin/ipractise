Suppose that you have a set of data structured like below:

<img src="Wrangling_tidyr_002_files/figure-markdown_strict/unnamed-chunk-1-1.png" height="20%" />

where the value name *sub* indicates wellbeing, *anx* indicates anxiety, and
the number indicates the time being measured. You may notice that the
time variable has both information about the measure (sub versus anx)
and time (1-3).

Which of the following functions is going to help break the time
variable into two variables, as illustrated below?

<img src="Wrangling_tidyr_002_files/figure-markdown_strict/unnamed-chunk-2-1.png" height="20%" />

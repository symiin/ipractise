Q7 [out of 20]. Suppose that you have data structured like below:

<img src="Wrangling_tidyr_002_files/figure-markdown_strict/unnamed-chunk-2-1.png" height="20%" />

where the value name *sub* indicates wellbeing, *anx* indicates anxiety, and
the number indicates the time being measured. You notice that the information about time and measure are separated into two variables.

Which of the following functions is going to help paste together the values of multiple columns into a single column, as illustrated below?

<img src="Wrangling_tidyr_002_files/figure-markdown_strict/unnamed-chunk-1-1.png" height="20%" />


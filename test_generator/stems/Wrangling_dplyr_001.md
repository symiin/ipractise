Consider the screenshot below.
<br>
<img src="Wrangling_dplyr_001_files/figure-markdown_strict/dplyr_001.png" height="1%" 
width = 70%/>

This screenshot shows selected properties of the dataset, <strong>univbct</strong>, previously published by Bliese and Ployhart (2002). Data were collected at three time points. Here we have a data frame with 19 columns from 495 individuals.

Suppose I would like to create a subset of the data, consisting only of men (GENDER = 1) from
Company A. What values should I substitute into the blank spaces to complete this function?

<p>
<pre><code>
company_A_men <- ___<strong>(i)</strong>___ (univbct, COMPANY ___<strong>(ii)</strong>___"A" ___<strong>(iii)</strong>___ GENDER ___<strong>(ii)</strong>___ 1)
</code></pre>
</p>

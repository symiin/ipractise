Q3 [out of 20]. Suppose that you want to compute average wellbeing score for subject 1
from the data below:

<img src="Wrangling_Semantic_004_files/figure-markdown_strict/unnamed-chunk-1-1.png" height="50%" />

Which of the following two chunks of code would help you get the result below?

*Option A*
<pre><code>df_long %>%
 filter(subid == 1) %>%
 dplyr::summarize(avg.well = mean(wellbeing)) %>%
 arrange(desc(avg.well))
</code></pre>
*Option B*
<pre><code>arrange(dplyr::summarize(filter(df_long, subid == 1), avg.well = mean(wellbeing)), desc(avg.well))
</code></pre>


**Result**:
<pre><code>
##   avg.well  <br>
## 1  6.13825
</code></pre>



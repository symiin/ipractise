# mirtCAT arguments related to UI
# See mirtCAT help page.
# change aesthetics of GUI, including title, authors, header, and initial message
testset <- read.csv(file ='./Assessment/test_archive/Rdemo.csv', header = T, stringsAsFactors = F)

title <- "Rdemo"
authors <- "iPRACTISE team"
firstpage <- list(h2("Example Test"), h5("Please answer each item to the best of your ability. ",        
                                         "The results of this test will remain completely anonymous ", 
                                         "and are only used to improve the system."))

begin_message <- "Please click on 'Next' on the left panel"


#Modify lastpage object
## remember that 'person' object is where mirtCAT saves user's responses
lastpage <- function(person){ 
  df <- data.frame(theta=person$thetas_history, id=1:nrow(person$thetas_history))
  df <- tidyr::gather(df, "factor", 'theta',  -id)
  gg <- ggplot(df, aes(id, theta)) + geom_point() + geom_line() + facet_wrap(~factor) +
    xlab('Trial #')+
    scale_x_continuous(breaks = seq(0, 30, 2))+
    ylab('Estimated Ability') +
    theme(axis.text=element_text(size=30),
          axis.title=element_text(size=40,face="bold"))
  
  filename <- './www/lastpage.png'
  #paste0(dirname, '/lastpage.png') # 'www' folder is what we gives to the server
  ggsave(filename, width = 16, height = 10, dpi='retina')
  
  weak_nodes <-unique(na.omit(c(miss$Topic1,miss$Topic2)))
  Q <- paste0("Please review materials in the following nodes: ", paste(weak_nodes, collapse = ', '))
  

  list( useShinyalert(),
        actionButton(inputId='demo', label="Recommendation",
                     icon = icon("pen"),
                     onclick = paste0('alert(','"',Q,'"',')')),

       h2("Thank you for completing the test."),
       h3("You can scroll down to see the summary results"), 
       h4('Your final ability estimates are:'),
       h4(sprintf("Theta score: %.3f, indicating that you are performing at about %.0fth percentile in the group taking this assessment", person$thetas[1], round(pnorm(person$thetas[1])*100,1))),
       # h4(sprintf("Ability two: %.3f, SE = %.3f", person$thetas[2],
       #            person$thetas_SE_history[nrow(person$thetas_SE_history), 2])),
       img(src=filename, height="400", width="600", align= 'center'))
       # print(getwd()), #locate the file location
       # h3("Now you can close this window and go back to the main app"),
       # h3("In the main app, click on 'move on' button to proceed") )
}

demographics <- list(
  textInput(inputId = "id", label = "[Required] Please indicate your PSU email account", value = "", placeholder = "xyz365@psu.edu"),
  radioButtons(inputId = 'consent', label = "[Required] Informed Consent: Would you be willing to share your responses for educational purpose?",
               choices = c('Yes' = 'Yes', 'No' = 'No'))
)

shinyGUI_list <- list(title = title, authors = authors, 
                      instructions = c("Click 'Next' to move to the next question", "Next"),
                      demographics = demographics,
                      demographics_inputIDs = c("id",'consent'), 
                      firstpage = firstpage, lastpage = lastpage,
                      begin_message = begin_message)

design_list <- list(max_items = 10, min_SEM = 0.6, delta_thetas = .1)

final_fun <- function(person){
  time_rs <- format(Sys.time(), "%y-%m-%d_%H_%M" )
  filename2 <- paste0('./www/', as.character((person$demographics)$id),'_', time_rs, "_Rdemo.rds")
  saveRDS(person, filename2)
  ind <- which(person$scored_responses==0)
  miss <<- testset[ind,]
  missItemfilename <<- paste0('./www/', as.character((person$demographics)$id),  "_Rdemo.csv")
  write.csv(testset[ind,], file=missItemfilename, row.names = F)
}

pars <-  data.frame(d = (testset$Difficulty), a1 = 1)
mo <- generate.mirt_object( parameters = pars,
                            itemtype = 'Rasch')
criteria <- if (isTruthy(mo)) {c('MI')} else c('random')


# mirtCAT arguments related to UI
# See mirtCAT help page.
# change aesthetics of GUI, including title, authors, header, and initial message
load(file ='./Assessment/test_archive/Regression.rdata')

title <- "HDFS519 - Midterm"
authors <- "Sy-Miin Chow"
firstpage <- list(h2("*** PLEASE READ ***"), 
                  h5("This portion of the exam will take approximately one to two hours of your time. Due to the nature of the test package we are using, you cannot go back to earlier questions and only the first set of responses will be graded. So only start this portion of the exam when you are ready."))

begin_message <- "Please click on 'Next' on the left panel to start"


#Modify lastpage object
## remember that 'person' object is where mirtCAT saves user's responses
lastpage <- function(person) {
  list(h3("Thank you for completing the iPRACTISE portion of the mid-term exam. Please answer the rest of the exam directly in R Markdown, R, or a text editor such as Word."))
}

# lastpage <- function(person){ 
#   df <- data.frame(theta=person$thetas_history, id=1:nrow(person$thetas_history))
#   df <- tidyr::gather(df, "factor", 'theta',  -id)
#   gg <- ggplot(df, aes(id, theta)) + geom_point() + geom_line() + facet_wrap(~factor) +
#     xlab('Trial #')+
#     scale_x_continuous(breaks = seq(0, 30, 2))+
#     ylab('Estimated Ability') +
#     theme(axis.text=element_text(size=30),
#           axis.title=element_text(size=40,face="bold"))
#   
#   filename <- './www/lastpage.png'
#   #paste0(dirname, '/lastpage.png') # 'www' folder is what we gives to the server
#   ggsave(filename, width = 16, height = 10, dpi='retina')
#   
#   list( actionButton(inputId='redirect', label="Review your test",
#         icon = icon("pen"),
#         onclick = "showNotification('blah blah', type = 'message')"),
# 
#        h2("Thank you for completing the test."),
#        h3("You can scroll down to see the summary results"), 
#        h4('Your final ability estimates are:'),
#        h4(sprintf("Theta score: %.3f, indicating that you are performing at about %.0fth percentile in the group taking this assessment", person$thetas[1], round(pnorm(person$thetas[1])*100,1))),
#        # h4(sprintf("Ability two: %.3f, SE = %.3f", person$thetas[2],
#        #            person$thetas_SE_history[nrow(person$thetas_SE_history), 2])),
#        img(src=filename, height="400", width="600", align= 'center'))
#        # print(getwd()), #locate the file location
#        # h3("Now you can close this window and go back to the main app"),
#        # h3("In the main app, click on 'move on' button to proceed") )
# }

demographics <- list(
  textInput(inputId = "id", label = "Enter your PSU access ID: ", value = ""))

shinyGUI_list <- list(title = title, authors = authors, 
                      instructions = c("Click 'Next' to move to the next question", "Next"),
                      demographics = demographics,
                      demographics_inputIDs = c("id"), 
                      firstpage = firstpage, lastpage = lastpage,
                      begin_message = begin_message)

# 
# design_list <- list(max_items = 10, min_SEM = 0.6, delta_thetas = .1)

final_fun <- function(person){
  time_rs <- format(Sys.time(), "%y-%m-%d_%H_%M" )
  filename2 <- paste0('./www/', as.character((person$demographics)$id),'_', time_rs, "_Regression.rds")
  saveRDS(person, filename2)
  ind <- which(person$scored_responses==0)
  miss <<- testset[ind,]
  missItemfilename <<- paste0('./www/', as.character((person$demographics)$id),  "_Rdemo.csv")
  write.csv(testset[ind,], file=missItemfilename, row.names = F)
}


mo <- NULL
criteria <- if (isTruthy(mo)) {c('MI')} else c('random')
start_item <- 1
design_list <- NULL

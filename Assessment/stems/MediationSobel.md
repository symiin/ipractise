The researcher would like to analytically compute the standard error for testing the indirect effect of <strong>creative</strong> on <strong>cheat</strong> through <strong>lie</strong> using a normal distribution to approximate the sampling distribution of the indirect effect. 

Plugging in the <strong> numerical values from the output rounded to two decimal places </strong>, provide the code for calculating the standard error using these values.  

<img src="Figures/figure-markdown_strict/MediationOutput.png" width="120%" />


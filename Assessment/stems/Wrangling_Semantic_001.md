Given data.frame **midwest**, the code for looking at the first 5
records is:

<pre><code>head(midwest, n = 5)
</code></pre>

<img src="Wrangling_Semantic_001_files/figure-markdown_strict/header-2.png" height="20%" />

Which one of the followings is NOT true about the data structure?

Suppose that we have an object named **C** that has the following
structure:

<pre><code> str(C)
</code></pre>

    ## 'data.frame':    24 obs. of  2 variables:
    ##  $ V1: num  1 7 24 5 22 11 18 9 15 6 ...
    ##  $ V2: num  0.44617 -0.40363 0.00633 -0.39043 0.05117 ...

and we want to generate the new dataset **D**, by manipulating **C**,
such as:

    ## 'data.frame':    24 obs. of  2 variables:
    ##  $ V1: num  1 2 3 4 5 6 7 8 9 10 ...
    ##  $ V2: num  0.446 1.475 -0.622 0.829 -0.39 ...

Which of the followings allows to reorder **C** as above?

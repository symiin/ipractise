Babies born at a Boston Massachusetts hospital that are under 1500 grams in weight are placed into a special care protocol.  A scatterplot for 100 of these very low birth weight infants shows how their birth weight and gestational age (length of pregnancy in weeks) are related.  Using the data in the scatterplot, 

<img src="Figures/figure-markdown_strict/IntroStat_1.png" style = "display:block; margin:0 auto;" />

an investigator computed two regression lines: 

<code> Gestational Age = 22.08 + 0.0062*Birth weight  </code>

and 

<code> Birth weight = -932.4 + 70.31*Gestational Age  </code>

For all of the births at this hospital (not just the ones in the special care protocol), the relationship between the number of cigarettes that a mother smokes during pregnancy and the birthweight has a correlation of -0.38. This means
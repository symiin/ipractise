A teacher keeps track of the time it took her students to complete a particular exam (in minutes). These times are recorded in the table below.

<img src="Figures/figure-markdown_strict/GOALS_12T.png" style = "display:block; margin:0 auto;" />

Each of the graphs below presents a valid representation of the time taken to complete the exam. Which of the graphs is the most appropriate display of the distribution of the times, in that the graph allows the teacher to describe the shape, center, and variability of the completion times?

<table style = "margin: auto;">
  <tr>
    <td><figure>
<figcaption style = "color:red; text-align:center;">
     A
</figcaption>
<img src="Figures/figure-markdown_strict/GOALS_12A.png" style = "display:block; margin:0 auto;" />
</figure></td>
<td></td>

    <td><figure>
<figcaption style = "color:red; text-align:center;">
      B
</figcaption>
<img src="Figures/figure-markdown_strict/GOALS_12B.png" style = "display:block; margin:0 auto;" />
</figure></td>
<tr>
<tr>
    <td><figure>
<figcaption style = "color:red; text-align:center;">
      C
</figcaption>
<img src="Figures/figure-markdown_strict/GOALS_12C.png" style = "display:block; margin:0 auto;" />
</figure></td>
<td></td>
<td><figure>
<figcaption style = "color:red; text-align:center;">
      D
</figcaption>
<img src="Figures/figure-markdown_strict/GOALS_12D.png" style = "display:block; margin:0 auto;" />
</figure></td>

  <tr>
</table>

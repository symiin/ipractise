---
#output: html_document
output: md_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
library(ggplot2)
data("midwest", package = "ggplot2")
midwest <- midwest[midwest$poptotal <= 2000000,]
```

Given data.frame **midwest**, the code for creating a scatterplot of **area** (x axis) vs. **poptotal** (y axis), differentiated by the variable **state** with different colors is:

<pre><code>ggplot(midwest, aes(x=area, y=poptotal)) + <br>
  geom_point(aes(colour=state))</code></pre>

```{r scatter, out.width='60%'}
ggplot(midwest, aes(x=area, y=poptotal)) + 
  geom_point(aes(colour=state))
```

You would like to add loess line more by the variable **popdensity** with different sizes.

```{r, out.width='60%'}
ggplot(midwest, aes(x=area, y=poptotal)) + 
  geom_point(aes(col=state)) +
  geom_smooth(method="loess", se=FALSE)
```

<pre><code>ggplot(midwest, aes(x=area, y=poptotal)) + <br>
  geom_point(aes(color=state, <span style="color:red">___</span>=popdensity))
</code></pre>

What would fill in the blank space above? [Click here](https://ggplot2.tidyverse.org/reference/ "ggplot2") 
to see the ggplot2 reference.





 




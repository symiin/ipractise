The following code is used to generate the histogram below:

<pre><code>hist(rnorm(500, 0, 1), xlab = 'Test Scores', main = 'Histogram of Test Scores')
</code></pre>

<img src="plotting_01_files/figure-markdown_strict/Hist.png" width="60%" />

What additional line of code must be included below to add the vertical blue line?

<pre><code>hist(rnorm(500, 0, 1), xlab = 'Test Scores', main = 'Histogram of Test Scores')<br>
<span style="color:red">______</span>(v = 0, lwd = 4, col = 'blue', lty = 2)
</code></pre> 

Suppose that you have two objects **A** and **B**, each of which has the
following structure :

<pre><code> head(A)
</code></pre>

    ## [1] 1 2 3

<pre><code> head(B)
</code></pre>

    ## [1] 4 5 6

What function would you use to stack two objects as the following:

<pre><code> 
    ##   [,1] [,2] [,3]<br>
    ## A    1    2    3<br>
    ## B    4    5    6
</code></pre>

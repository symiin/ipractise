On the first day of her statistics class, Dr. Smith gave students a pretest to determine their statistical knowledge. At the end of the course, she gave students the exact same test. Dr. Smith constructed the scatterplot (below on the left) between students' pretest and posttest scores. The solid point in the upper right corner of the scatterplot represents the pretest and posttest scores for John. It turns out that John's pretest score was actually 5 (not 100 as previously recorded), and his posttest score was 100. John's scores were corrected and a new scatterplot was constructed (below on the right).


<img src="Figures/figure-markdown_strict/GOALS_4.png" style = "display:block; margin:0 auto;" />

How would you expect the strength of the correlation between the pretest and posttest scores for the new scatterplot with John's actual scores (above, right) to compare to the strength of the relationship for the original scatterplot (above, left)?

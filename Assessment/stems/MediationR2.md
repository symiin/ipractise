<pre><code>	The output from running the analysis is shown below. 
</code></pre>
<img src="Figures/figure-markdown_strict/MediationOutput.png" width="120%" />

What proportion of variance in likelihood to cheat is explained collectively by creativity and openness to lying?

The following code is used to generate the network below on the left:

<pre><code>qgraph(NET, theme = 'gimme')
</code></pre>

<img src="nets_01_files/figure-markdown_strict/Networks.png" width="60%" />

What argument needs to be added to the code to produce the figure on the right?

<pre><code>qgraph(NET, theme = 'gimme', _______)
</code></pre>

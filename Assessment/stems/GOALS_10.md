Imagine a candy company that manufactures a particular type of candy where 50% of the candies are red. The manufacturing process guarantees that candy pieces are randomly placed into bags. The candy company produces bags with 20 pieces of candy and bags with 100 pieces of candy.

Which pair of distributions (below) most accurately represents the variability in the percentage of red candies in an individual bag that would be expected from many different bags of candy for the two different bag sizes?

<figure>
<figcaption style = "color:red; text-align:center;">
    Pair A
</figcaption>
<img src="Figures/figure-markdown_strict/GOALS_10A.png" style = "display:block; margin:0 auto;" />
</figure>

<br><br>
<figure>
<figcaption style = "color:red; text-align:center;">
    Pair B
</figcaption>
<img src="Figures/figure-markdown_strict/GOALS_10B.png" style = "display:block; margin:0 auto;" />
</figure>

<br><br>
<figure>
<figcaption style = "color:red; text-align:center;">
    Pair C
</figcaption>
<img src="Figures/figure-markdown_strict/GOALS_10C.png" style = "display:block; margin:0 auto;" />
</figure>
This question asks you to think about factors that might affect the width of a confidence interval. A confidence interval is shown as a horizontal line. The sample mean is represented by a solid dot in the middle of the confidence interval.

Imagine that two different random samples of test scores are drawn from a population of thousands of test scores. The first sample includes 250 test scores and the second sample includes 50 test scores. A 95% confidence interval for the population mean is constructed using each of the two samples.

Which set of confidence intervals (below) represents the two confidence intervals that would be constructed?

<table style = "margin: auto;">
  <tr>
    <td><figure>
<figcaption style = "color:red; text-align:center;">
    Set A
</figcaption>
<img src="Figures/figure-markdown_strict/GOALS_11A.png" style = "display:block; margin:0 auto;" />
</figure></td>

    <td><figure>
<figcaption style = "color:red; text-align:center;">
     Set B
</figcaption>
<img src="Figures/figure-markdown_strict/GOALS_11B.png" style = "display:block; margin:0 auto;" />
</figure></td>

    <td><figure>
<figcaption style = "color:red; text-align:center;">
     Set C
</figcaption>
<img src="Figures/figure-markdown_strict/GOALS_11C.png" style = "display:block; margin:0 auto;" />
</figure></td>

  <tr>
</table>

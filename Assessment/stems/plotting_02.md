The following code is used to generate the line graph below:

<pre><code>plot(Happy, Cal, xlab = 'Happiness', ylab = 'Calories Consumed')
</code></pre>

<img src="plotting_02_files/figure-markdown_strict/Line.png" width="60%" />

In the added code below, what do the arguments: **a** and **b** represent?

<pre><code>hist(rnorm(500, 0, 1), xlab = 'Test Scores', main = 'Histogram of Test Scores')<br>
abline(a = 0, b = 0.8, lwd = 4, col = 'blue', lty = 2)
</code></pre> 

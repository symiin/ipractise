Suppose that you have two datasets, **A** and **B**, that include county
names and total population, as the following:

<pre><code> head(A); head(B)
</code>
</pre>

    ##      county poptotal
    ## 1     ADAMS    66090
    ## 2 ALEXANDER    10626
    ## 3      BOND    14991
    ## 4     BOONE    30806
    ## 5     BROWN     5836

    ##       COUNTY poptotal
    ## 7    CALHOUN     5322
    ## 8    CARROLL    16805
    ## 9       CASS    13437
    ## 10 CHAMPAIGN   173025
    ## 11 CHRISTIAN    34418
    ## 12     CLARK    15921

Say you are going to create a new dataset by merging those above. Then
you found that the first column names do not match even though they
represent the same type of data. With **merge()** function as below,

<pre><code> C <- merge(x = A, y = B, _________)
</code>
</pre>

what arguments would help to consider *county* and *COUNTY* as being
mached?

[Click
here](https://www.rdocumentation.org/packages/data.table/versions/1.14.0/topics/merge)
to see the documentation of merge function.

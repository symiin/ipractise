---
#output: html_document
output: md_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
library(ggplot2)
data("midwest", package = "ggplot2")
```

Given data.frame **midwest**, the code for creating a scatterplot of **area** (x axis) vs. **poptotal** (y axis) is:  

<pre><code>ggplot(midwest, aes(x=area, y=poptotal)) + <br>
  geom_point()
</code></pre>

```{r scatter, out.width='60%'}
ggplot(midwest, aes(x=area, y=poptotal)) + 
  geom_point()
```

You need to differentiate the points by the variable **state** with different colors.

```{r, out.width='60%'}
ggplot(midwest, aes(x=area, y=poptotal)) + 
  geom_point(aes(col=state))
```

<pre><code>ggplot(midwest, aes(x=area, y=poptotal)) + <br>
  geom_point(aes(<span style="color:red">___</span>=state))
</code></pre>

What would fill in the blank space above? [Click here](https://ggplot2.tidyverse.org/reference/ "ggplot2") 
to see the ggplot2 reference.





 




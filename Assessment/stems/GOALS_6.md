A researcher investigated the impact of a particular herbicide on the enzyme level of carbonyl reductase in fish. In the study, 60 farm-raised fish were randomly assigned to the treatment group (in which they were exposed to the herbicide) or to the control group (in which they were not exposed to the herbicide). There were 30 fish assigned to each group. After the study, the data were analyzed, and the results of that analysis are reported in the output below:


<img src="Figures/figure-markdown_strict/GOALS_6.png" style = "display:block; margin:0 auto;" />

indicate if the following statement is a valid or invalid inference that can be made from the study results.

<br>
<i><b>"Based on the results of this study, the researchers should not conclude that the herbicide has an effect on the enzyme levels of farm-raised fish." </b></i>

<For current and future iPRCTISE collaborator>

This folder includes the item bank that we've collected for the assessment part of iPRACTISE app, as well as some by-products during the integration of items from various sources. The main contributors are
 - Dennis Pearl: Professor in Statistics at PennState
 - Jungmin Lee: Graduate RA, PennState (leejapply@gmail.com, shoot me an email if you have questions about this folder)
 - TJ Schaffer: Undergraduate RA, PennState

I, Jungmin Lee, leave some notes as the following that may help others to update and maintain the itembank

<Tips that would be immediate help>

1. DON'T USE MS Excel when editing '.csv' source file: it automatically and inconsistently removes decimal points that end with 0s from your source file, which has been a known issue in Excel community. It still doesn't have an easy solution (unless you have knowledge about Excel Macro), so I would recommend to use other editors that relatively better handle .csv type file (e.g. https://www.moderncsv.com/)

2. USE 'test_archive/design.r' as a start point to customize settings for the implementation of mirtCAT test, and save and display of the test results. For example, 'STAT414.R' includes the configuration information that sources 'STAT414.rdata' as an itembank for the adaptive test

3. It is always good to save the previous version of 'itembank.csv' separately before integrating it with new items


<Some notes>
4/2/21

Note from trying to convert the PDF of SOA questions to R dataframe

- instead of manual copy and pastes, I tried to read and parse the texts from the PDF and found some issues

A. Exporting PDF to R doesn't keep well the tables and equations
B. Using question number with space as a parsing keyword is not a perfect way to split the texts by questions (see Q43 as an example by reading 'edu-exam-p-sample-quest.pdf')

Conclusion: should do a hybrid approach that 

-First, use the automatic parsing
-Second, review and manually merge the unintentionally segregated texts
-Third, review and edit questions that require math equations or tables
---
title: "iPRACTISE"
# resource_files:
# - Math-stem.html
runtime: shiny
output: flexdashboard::flex_dashboard
---

```{r setup, echo=FALSE, message=FALSE}
knitr::opts_chunk$set(echo = FALSE)
library(mirtCAT)
library(data.tree)
library(collapsibleTree)
library(dplyr)
require(colorspace)
# use shinyjs in rmarkdown document
library(shinyjs)
useShinyjs(rmd = TRUE)
```

Topic
===================================== 
```{r tree}
shinyAppDir("./hierarchyPlot",
            options = list(width = "100%", height = 700))
```

Assessment
===================================== 
```{r model, eval=FALSE}
library(DBI)
# Create an ephemeral in-memory RSQLite database
#con <- dbConnect(RSQLite::SQLite(), ":memory:")
#dbListTables(con)

set.seed(1234)
nitems <- 120
itemnames <- paste0("Item.", 1:nitems)
a <- matrix(c(rlnorm(nitems/2, 0.2, 0.3), rnorm(nitems/4, 0, 0.3), numeric(nitems/2), 
  rnorm(nitems/4, 0, 0.3), rlnorm(nitems/2, 0.2, 0.3)), nitems)
d <- matrix(rnorm(nitems))
pars <- data.frame(a, d)
colnames(pars) <- c("a1", "a2", "d")
trait_cov <- matrix(c(1, 0.5, 0.5, 1), 2, 2)

# create mirt_object
mod <- generate.mirt_object(pars, itemtype = "2PL",
                            latent_covariance = trait_cov)
```

```{r question}
source("items/items_visual_ggplot.R")
#source("items/Example-5.R")
```


```{r design, eval=FALSE}
#design_list <- list(max_items = 10, min_items = 5, min_SEM = 0.9)

# in pre-CAT stage select 5 items using DPrule and use EAP estimates
#preCAT_list <- list(max_items = 5, criteria = "DPrule", method = "EAP")
```

```{r mirtUI, eval=TRUE}
# change aesthetics of GUI, including title, authors, header, and initial message
title <- "iPRACTISE Computerized Adaptive Test on R"
authors <- "iPRACTISE team"
firstpage <- list(h2("Example Test"), h5("Please answer each item to the best of your ability. ",        
                                         "The results of this test will remain completely anonymous ", 
                                         "and are only used for research purposes."))
lastpage <- function(person){
  list(h2("Thank you for completing the test."), 
       h4('Your final ability estimates are:'),
       h4(sprintf("Ability one: %.3f, SE = %.3f", person$thetas[1],
                  person$thetas_SE_history[nrow(person$thetas_SE_history), 1])))
       #h4(sprintf("Ability two: %.3f, SE = %.3f", person$thetas[2],
      #            person$thetas_SE_history[nrow(person$thetas_SE_history), 2])),
}
demographics <- list(textInput(inputId = "occupation", label = "What is your occupation?",
                               value = ""), selectInput(inputId = "gender", label = "Please select your gender.",
                                                        choices = c("", "Male", "Female", "Other"), selected = ""))
shinyGUI_list <- list(title = title, authors = authors, demographics = demographics,
                      demographics_inputIDs = c("occupation", "gender"), firstpage = firstpage, lastpage = lastpage)

#inputs <- readRDS("inputs.rds")
#if (inputs$caption == "") runi <- FALSE else runi <- TRUE 

```

```{r mirtcat}
mirtCAT_preamble(df=df, mo=mod, criteria="MEPV",
                 design=design_list, start_item="random",
                 shinyGUI=shinyGUI_list)
#mirtCAT_preamble(df = df, mo = mod, criteria = "Drule", start_item = "DPrule",
#				   shinyGUI = shinyGUI, design=design)
createShinyGUI()
```



---
title: "Muitidimensional IRT-Tutorial"
author: "Jungmin Lee"
date: '2020 3 25 '
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Introduction
This is a tutorial for someone to use 'mirt' package to implement IRT models. Some parts of the scripts are from Donjun's previous work on 'mirtcat_for_developr.R' (in the same shared folder)

Loading libraries
```{r}
library(mirt)
library(mirtCAT)
library(plyr)
```

# General Info
1. 'mirt' object must be privided to mirtCAT to run adative test, mainly because the mirt object contains IRT parameters
2. At first, you wouldn't have responses from participants to estimate parameters. You shoul set up (prior) population parameters.

3. Possible models may be 2-4PL model,

$$p(Y=1|\theta, a, d, g) = g + \frac{(u - g)}{ 1 + exp(-(a_1 * θ_1 + a_2 * θ_2 + d))}$$
where 
<!-- (prior) parameter setting -->
<!-- The reserved terms for 'mirt' -->
<!--   - a1, a2: discrimination -->
<!--   - d: difficulty -->
<!--   - g: guess -->

# Model specification 

##tutorial
Initial parameters setting
```{r}
# Two factor IRT assumed.
# parameters for the item 1 (3PL)
par1 <- data.frame(a1 = -1, a2 = 1, d = -1, g = 0.2)
# parameters for the item 2: related to the 1st factor (2PL)
par2 <- data.frame(a1 = 1, d = -1)
# parameters for the item 3: related to the 2nd factor (2PL)
par3 <- data.frame(a2 = 1, d = 1)
# parameters for the item 4: related to the 2nd factor (3PL)
par4 <- data.frame(a2 = -1, d = 1, g = 0.3)
pars <- rbind.fill(par1, par2, par3, par4)

#notice the NA's where parameters do not exist
pars

```

generate mirt object from the givin IRT parameters
```{r}
itemtype <- c('3PL','2PL','2PL','3PL')
mo <- generate.mirt_object(
  pars, itemtype = itemtype)
plot(mo, type = 'trace')
coef(mo)
class(mo)
extract.mirt(mo, 'model')
extract.mirt(mo, 'itemtype')
```
Caution: generate.mirt_object() seemingly remove model specification
So, work with 'mirt' directly to estimate parameters.


## Fit IRT using mirt::mirt with simulated data
generate (possible) responses from the givin IRT parameters 'mo'
```{r}
response_prior <- generate_pattern(
  # not perfect, but this is for the start. better than nothing
  mo = mo,
  # random ability scores (2 factors) for 100 participants 
  Theta = matrix(rnorm(200), nrow=100))
colnames(response_prior) <- paste0("item.", 1:4)
head(response_prior)
```

generate model object (See mirt help page)
```{r}
# lambda matrix: 1 for estimation
lambda <- matrix(c(1, 1,
                   1, 0,
                   0, 1,
                   0, 1), ncol=2, byrow=TRUE,
                 dimnames = list(NULL, c('F1', 'F2')))
cov_f <- matrix(c(FALSE, TRUE, TRUE, FALSE), 2)
model_mirt <- mirt.model(lambda, COV=cov_f)
model_mirt

mo1 <- mirt(data = response_prior, model=model_mirt, 
            itemtype = itemtype)

```

```{r}
# check the model specification is correct
extract.mirt(mo1, 'model')
extract.mirt(mo1, 'itemtype')
coef(mo1)
```

## Fitting 1PL-3PL model to empirical data

read raw data
```{r}
df <- read.csv(file ='sample_df.csv', header = T)
head(df)
```

Seems that we need to edit and transform the data accordingly to wide format (person x Item)

```{r}
df$Qid <- as.numeric(factor(df$Description))

iteminfo <- data.frame(itemnum= 1:109, description= levels(factor(df$Description) ))
```

subsetting variables and reshaping dataset 
```{r}
df.long <- df[,c('Actor', 'Qid','Correct.true.')]
colnames(df.long) <- c('ID','Qid','Item')
df.long$Item <- as.numeric(df.long$Item)
df.long <- df.long[order(df.long$Qid),]
df.wide <- reshape(df.long, idvar = "ID",
                timevar = "Qid", direction = "wide")
rownames(df.wide) <- NULL
head(df.wide)
```

Model Implementation
```{r}
data <- data.frame(df.wide[,-1])
str(data)

(mod1 <- mirt(data, 1)) #error due to some questions having only 1 type of response

missing <- c('Item.14','Item.45','Item.47','Item.79','Item.83', 'Item.85', 'Item.90', 'Item.92', 'Item.95', 'Item.97', 'Item.107', 'Item.108' )
data2 <- data[,-match(missing , colnames(data))]

(mod0 <- mirt(data2, 1, itemtype = 'Rasch')) # 1factor, 1PL
# (mod1 <- mirt(data2, 1)) #1factor model, 2PL
# (mod2 <- mirt(data2, 2)) #2factor model, 2PL
# (mod3 <- mirt(data2, 2, itemtype = '3PL')) #2factor model, 3PL

```

model comparison
```{r}
anova(mod0, mod1) # 1factor 1PL vs 2PL
anova(mod1, mod2) # 1factor 2PL vs 2factor 2PL
anova(mod2, mod3) # 2factor 2PL vs 3PL
```
1factor 1PL has the lowest AIC and BIC, and the loglikelihood test indicates that 2f 3PL doesn't have better explanation than 2PL

Results 
```{r}
summary(mod0)
coef(mod0) # Item estimates
plot(mod0, type = 'trace')
plot(mod0)
itemplot(mod0, item = 1, auto.key = list(space = 'right'))
#residuals(mod0)

```

Figures
```{r}
est.difficulty <- coef(mod0)
est.theta <- fscores(mod0) # Person ability estimates
hist(est.theta)

i <- names(mod0@Data$mins)
est.items <- NULL

for (x in i) {
    est.items <- rbind(est.items, est.difficulty[[x]])
}
rownames(est.items) <- i
hist(est.items[,2], main = "Histogram of Estiamted Item Difficulty")
```

# Save results
```{r}
Items <- data.frame(est.items)
Items$description <- iteminfo[substr(i, 6,10), 2]
Users <- data.frame(UserID = df.wide$ID, Theta = est.theta)

write.csv(df.wide, file = 'df_resp_old.csv', row.names = F)
write.csv(iteminfo, file = 'iteminfo_old.csv', row.names = F)
write.csv(Items, file = 'Items_Estimates.csv')
write.csv(Users, file = 'Users_Estimates.csv')
```

# Confirmatory model - 2factor 1PL
**4/2: got informed from Dennis that this data comes from two different applications having different learning goals. It maybe worth trying two-dimensional IRT with manual allocations of factor loadings **

Matching learning goals and items
```{r}
ind <- match( Items$description, df$Description)
Items$Activity <- df$Activity[ind]
```

Generating lamda matrix & cov_f
```{r}
v.mat <- as.numeric(Items$Activity=="Matching Distributions")
p.mat <- as.numeric(Items$Activity=="Probability Applications")

lambda2 <- matrix(c(v.mat, p.mat), ncol=2, byrow=F,
                 dimnames = list(NULL, c('F1', 'F2')))
cov_f <- matrix(c(FALSE, TRUE, TRUE, FALSE), 2)

mod_1PL_cfa <-  mirt.model(lambda2, COV=cov_f)
mod_1PL_cfa
```

Implementing 2factor 1PL & 2PL
```{r}
(mod4 <- mirt(data = data2, model = mod_1PL_cfa, itemtype = 'Rasch'))
summary(mod4)
(mod5 <- mirt(data = data2, model = mod_1PL_cfa, itemtype = '2PL',
              technical = list(NCYCLES=1000))) #didn't converge

anova(mod4,mod5)

summary(mod4)
coef(mod4)
fscores(mod4)
plot( fscores(mod4),
      main = 'Relationship Between two Factors',
      xlab="Matching Distributions", ylab="Probability Applications")
```


**4/3 Dennis pointed out that many students might have used one application but not the other. We decided to subset the items by the type of application and fit the separate model**

```{r}
vdat <- data2[,which(v.mat == 1)] #76 items 
pdat <- data2[,which(p.mat == 1)] #21 items

(mod4.1 <- mirt(data = vdat, model = 1, itemtype = 'Rasch',
                technical = list(removeEmptyRows=T))) #37 students

(mod4.2 <- mirt(data = pdat, model = 1, itemtype = 'Rasch', #16 students
                technical = list(removeEmptyRows=T)))

summary(mod4)
summary(mod4.1) # no changes in factor loadings
summary(mod4.2) # slightly larger factor loadings compared to model4

hist(fscores(mod4.1), main = "Matching Distribution")
hist(fscores(mod4.2), main = "Probablity Application")
coef(mod4.1)

range(fscores(mod4.2))
coef(mod4.2)

```

Comparisons mod4 vs mod4.1
```{r}
est.difficulty <- coef(mod4)
i <- names(mod4@Data$mins)
est.items.m4 <- NULL

for (x in i) {
    est.items.m4 <- rbind(est.items.m4, est.difficulty[[x]])
}
rownames(est.items.m4) <- i
hist(est.items.m4[colnames(vdat),'d'], main = "Estiamted Item Difficulties-2F 1PL")

est.difficulty <- coef(mod4.1)
i <- names(mod4.1@Data$mins)
est.items.m4.1 <- NULL

for (x in i) {
    est.items.m4.1 <- rbind(est.items.m4.1, est.difficulty[[x]])
}
rownames(est.items.m4.1) <- i
hist(est.items.m4.1[,'d'], main = "Estiamted Item Difficulties-Matching Distribution")

hist(est.items.m4[colnames(vdat),'d'], main = "Estiamted Item Difficulties-2F 1PL")

plot(est.items.m4.1[,'d'], est.items.m4[colnames(vdat),'d'],
     xlab = 'mod4.1', ylab = 'mod4')

cor(est.items.m4.1[,'d'], est.items.m4[colnames(vdat),'d'])
```

mod4 vs mod4.2
```{r}
est.difficulty <- coef(mod4.2)
i <- names(mod4.2@Data$mins)
est.items.m4.2 <- NULL

for (x in i) {
    est.items.m4.2 <- rbind(est.items.m4.2, est.difficulty[[x]])
}
rownames(est.items.m4.2) <- i
hist(est.items.m4.2[,'d'], main = "Estiamted Item Difficulties-Probability Distribution")

hist(est.items.m4[colnames(pdat),'d'], main = "Estiamted Item Difficulties-2F 1PL")

plot(est.items.m4.2[,'d'], est.items.m4[colnames(pdat),'d'],
     xlab = 'mod4.2', ylab = 'mod4')
```


```{r}
fscores(mod4)

fscores(mod4.1)
fscores(mod4.2)
```


# 4/17 run adaptive test with 'mod4.2'
Here, we are going to create a mirtCAT set that corresponds to the IRT model for probability application, which reads 'mod4.2' as a source.

Download csv from github
```{r}
library (readr)

filename = 'https://raw.githubusercontent.com/EducationShinyAppTeam/Probability_Applications/master/completeg.csv'

filename2 = 'https://raw.githubusercontent.com/EducationShinyAppTeam/Matching_Distributions/master/distributionG.csv'

mydata<-read_csv(url(filename)) #dat from probablity application app
mydata2 <- read_csv(url(filename2)) #dat from Matching_Distribution application app


```

##Data processing

Check if I can use match function for items description
```{r}
mydata$`Problem Description`

as.character(Items[colnames(pdat),'description'])
```
Seems that the item descriptions in the dataset are slightly different from what I have. I would like to remove the phrase 'Identify the distribution of given text:' as that doesn't show up in the problem description from the dataset

Remove redundant phrase
```{r}
id_item <- colnames(pdat)
keyword <- 'Identify the distribution of given text: '
Items$description <- as.character(Items$description)
Items[, 'description'] <- sub(pattern = keyword, replacement = '',Items$description )
match(Items[id_item, 'description'], mydata$`Problem Description`)

nchar(Items[id_item, 'description']) 
nchar(Items[id_item, 'description']) == 214
is.na(match(Items[id_item, 'description'], mydata$`Problem Description`))

```
Warning: Using match function is problematic because the items description seems to be trimmed when it is more than 214 characters. As a quick fix, I am going to use the first 214 characters from the description in the dataset to find mached items

match first 214 characters in the item description from iteminfo and dataset
```{r}
ref <- substr(mydata$`Problem Description`, 1, 214)

(ind.item <- match(Items[id_item, 'description'], ref)) # Seems to work!

testset <- as.data.frame(mydata[ind.item,c(2,4,5,6,7,10)]) #subset the items corresponding to the IRT model

str(testset)
```

Change colnames and add a few variables that should be specified for mirtCAT 
```{r}
colnames(testset) <- c('Question', paste0('Option.',1:4), 'Answers')
testset$Stem <- NA
testset$Type <- 'radio'
answerfuns <- as.list(rep(NA, 21))

```

## Deploy mirtCAT - adaptive test

load libraries
```{r setup, echo=FALSE, message=FALSE}
library(mirtCAT)
library(shiny)
library(shinyjs)
library(shinyBS)
library(shinydashboard)
sess.name = createSessionName(n = 50)

```

Front page
```{r mirtUI, eval=TRUE}
# mirtCAT arguments related to UI
# See mirtCAT help page.
# change aesthetics of GUI, including title, authors, header, and initial message
title <- "iPRACTISE Computerized Adaptive Test on R"
authors <- "iPRACTISE team"
firstpage <- list(h2("Example Test"), 
                  h5("Please answer each item to the best of your ability."))#,        
                                         #"The results of this test will remain completely anonymous ", 
                                         #"and are only used for research purposes."))
                                         
lastpage <- function(person) {
  list(h3("Thank you for completing the test."),
       h5("Please do NOT close the tab unless instructed to do so."))
}

demographics <- list(
  textInput(inputId = "id", label = "PSU ID", value = ""))

shinyGUI_list <- list(title = title, authors = authors, 
                      demographics = demographics,
                      demographics_inputIDs = c("id"), 
                      firstpage = firstpage, lastpage = lastpage)
```

additional options for adaptive test
```{r design, eval=TRUE}
# The 'design' argument of mirtCAT
# See mirtCAT help page.
design_list <- list(max_items = 10, min_SEM = 0.9)
```

creat mirtCAT object
```{r}
sess.name = createSessionName(n = 50)

mirtCAT_preamble(
  df = testset, mo = mod4.2, # <- identifying testset and model object
  AnswerFuns = answerfuns, # <- can be a list of 'NA' unless an item has specific grading rules
  criteria = 'MI', # indicates adaptive method, see'mirtCAT' help page for options
  start_item = 'MI', # decides the first item user takes, can be item number
  design = design_list, # additional setting such as maximum items to take, stopping criteria
  shinyGUI = shinyGUI_list, # specifying front page, demographics, and last page
  sessionName = sess.name # <- after version update, giving session number is a must

)
createShinyGUI(  sessionName = sess.name) # Generate shinyapp based on the above

results <- getPerson(sessionName)
summary(results, sort =T)
```

Save testset and model
```{r}
save(list = c('testset', 'mod4.2', 'answerfuns', 'shinyGUI_list', 'design_list', 'pdat'),
     file = 'testset.rdata')
```

# 4/24 update IRT parameters
Item estimates may need to be updated once we have enough response patterns from the data collection. The issue is whether we want 
1. 'real time' update - update response dataset everytime we get new record, fit the model again, and deploy the mirtCAT test to the server
2. 'delayed' update - send a user's response to the box folder to update the response dataset that will be used to estimate the new model in fixed amount of time (manually)

Which one is for us would depend on the workload for programming. Regardless, the general iterative procedure would like be below

##Essential steps for updating

1. save a user's response to a object
```{r}
person <- getPerson(sessionName) 
summary(person, sort=FALSE) # 'sort=FALSE' gets us the full length of scored responses vector

```

2. extract a vector of scored_response
```{r}
response <- person$scored_responses
```

3. add the new response to the existing response dataset
```{r}
pdat <- rbind(pdat, response) # Here, pdat is assumed an original dataset and we just added new record
```
 
4. fit an IRT model with updated dataset to update the item parameters
```{r}
(mo_new <- mirt(data = pdat, model = 1, itemtype = 'Rasch', #16 students
                technical = list(removeEmptyRows=T)))

coef(mo_new)
```

5. Re-run mirtCAT
```{r}
sess.name = createSessionName(n = 50)

mirtCAT_preamble(
  df = testset, mo = mo_new, # <- identifying testset and model object
  AnswerFuns = answerfuns, # <- can be a list of 'NA' unless an item has specific grading rules
  criteria = 'MI', # indicates adaptive method, see'mirtCAT' help page for options
  start_item = 'MI', # decides the first item user takes, can be item number
  design = design_list, # additional setting such as maximum items to take, stopping criteria
  shinyGUI = shinyGUI_list, # specifying front page, demographics, and last page
  sessionName = sess.name # <- after version update, giving session number is a must

)
createShinyGUI(  sessionName = sess.name) # Generate shinyapp based on the above
```
NOTE: The end of Step 5 should be connected to step 1

##Thoughts
If we want 'real-time' parameter updates, final function of mirtCAT would need to include step1-4 with pdat and mo_new appropriately saved to box. Additionally, mirtCAT assessment needs to be activated by loading such files from the box folder.
In the other case, including step1-3 would be enough for final function.
In both cases, securing Box authentication may be critical for this functionality

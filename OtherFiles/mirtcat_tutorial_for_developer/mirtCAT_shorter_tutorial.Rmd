---
title: "mirtCAT- short tutorial"
author: "Jungmin Lee"
date: '4/20'
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Introduction
This is a quick version of mirtCAT tutorial for someone to get general sense of relating mirt object to mirtCAT function. Some parts of the scripts are from Donjun's previous work on 'mirtcat_for_developr.R' (in the same shared folder)

Loading libraries and example dataset
```{r}
library(mirt)
library(mirtCAT)
library(plyr)
library(ggplot2)
load('testset.rdata')
sess.name = createSessionName(n = 50)
```

# General Info
1. 'mirt' object must be privided to mirtCAT to run adative test, mainly because the mirt object contains IRT parameters
2. At first, you wouldn't have responses from participants to estimate parameters. You shoul set up (prior) population parameters.

3. Possible models may be 1-4PL model, expressed below as a general form

$$p(Y=1|\theta, a, d, g) = g + \frac{(u - g)}{ 1 + exp(-(a_1 * θ_1 + a_2 * θ_2 + d))}$$
where Y=1 means that the current question gets hit(correct)
<!-- (prior) parameter setting -->
<!-- The reserved terms for 'mirt' -->
<!--   - a1, a2: item discrimination -->
<!--   - theta1, theta2: user ability -->
<!--   - d: item difficulty -->
<!--   - g: guess -->

Note that the number of thetas can be varied depending on your model.

# Model specification in mirt

Here, I will give 1PL model with 1 factor as an example.
Let's take a look at the object 'mod4.2' that is the mirt object I made,which has information about prior parameters
```{r}
#mod4.2
coef(mod4.2) [[1]] # Each test item has 4 attributes corresponding to the parameters above
```
You see here that the 'mod4.2' object has prior parameters info by items.

We can manually generate this model object by providing a parameters table

Example: assume we have 4 items corresponding to one of two factors(thetas)
```{r}
# Two factor IRT assumed.
par1 <- data.frame(a1 = -1, a2 = 1, d = -1, g = 0) #for item 1
par2 <- data.frame(a1 = 1, d = -1) #for item 2
par3 <- data.frame(a2 = 1, d = 1) #for item 3
par4 <- data.frame(a2 = -1, d = 1, g = 0) #for item 4

pars <- rbind.fill(par1, par2, par3, par4)

#notice the NA's where parameters do not exist
pars

```

generate mirt object from the givin IRT parameters
```{r}
itemtype <- 'Rasch' # Rasch is 1PL model that only has item difficulty parameter
mo <- generate.mirt_object(
  pars, itemtype = itemtype)
coef(mo)

```
Caution: generate.mirt_object() seemingly remove model specification
So, work with 'mirt' directly to estimate parameters if possible.


# Run adaptive test with 'mod4.2'
Here, we are going to create a mirtCAT set that corresponds to the IRT model for probability application, which reads 'mod4.2' as a source.

## The structure of mirtCAT function

Here is the general specification of mirtCAT function. I'll put annotations by lines
```{r, eval = FALSE}
mirtCAT_preamble(
  df = testset, mo = mod4.2, # <- identifying testset and model object
  AnswerFuns = answerfuns, # <- can be a list of 'NA' unless an item has specific grading rules
  criteria = 'MI', # indicates adaptive method, see'mirtCAT' help page for options
  start_item = 'MI', # decides the first item user takes, can be item number
  design = design_list, # additional setting such as maximum items to take, stopping criteria
  shinyGUI = shinyGUI_list, # specifying front page, demographics, and last page
  sessionName = sess.name # <- after version update, giving session number is a must

)
createShinyGUI(  sessionName = sess.name) # Generate shinyapp based on the above
```

## Practice - Deploy mirtCAT
Let's try creating a mirtCAT object step by step

load libraries
```{r setup, echo=FALSE, message=FALSE}
library(mirtCAT)
library(shiny)
library(shinyjs)
library(shinyBS)
library(shinydashboard)
```

1. generating testset - I'll use the existing one
```{r}
View(testset)
dim(testset)
colnames(testset)
```

**NOTE that**
- the number of rows in the testset should match the number of items in the model object
- all the variables shown in the dataframe are basic elements needed for the mirtCAT 


2. model to be used - we covered generating mirt object in the beginning

3. Answerfunction - This should be a list object. For now, let's make them have NAs. Again, the length should be matched with the total items.
```{r}
answerfuns <- as.list(rep(NA, nrow(testset)))
answerfuns[[1]]
```

4. design list - additional options for adaptive test. In this example, let's simply set the maximum number of items user would take and minimum standard error for stopping
```{r design, eval=TRUE}
# The 'design' argument of mirtCAT
# See mirtCAT help page for more possible options.
design_list <- list(max_items = 10, min_SEM = 0.9)
```

5. shinyGUI_list - this is realted to the user interface on the shinyapp, including front page, instructions, demographics, and last page
```{r mirtUI, eval=TRUE}
# mirtCAT arguments related to UI
# See mirtCAT help page.
# change aesthetics of GUI, including title, authors, header, and initial message
title <- "iPRACTISE Computerized Adaptive Test on R"
authors <- "iPRACTISE team"
firstpage <- list(h2("Example Test"), 
                  h5("Please answer each item to the best of your ability."))#,        
                                         #"The results of this test will remain completely anonymous ", 
                                         #"and are only used for research purposes."))
                                         
# lastpage <- function(person) {
#   list(h3("Thank you for completing the test."),
#        h5("Please do NOT close the tab unless instructed to do so."))
# }

demographics <- list(
  textInput(inputId = "id", label = "PSU ID", value = ""))

shinyGUI_list <- list(title = title, authors = authors, 
                      demographics = demographics,
                      demographics_inputIDs = c("id"), 
                      firstpage = firstpage, lastpage = lastpage)
```

6. create session and run mirtCAT object
```{r}
sess.name = createSessionName(n = 50)

mirtCAT_preamble(
  df = testset, mo = mod4.2,
  AnswerFuns = answerfuns,
  sessionName = sess.name, 
  criteria = 'MI',start_item = 'MI',
  shinyGUI = shinyGUI_list, design = design_list
)
createShinyGUI(sessionName = sess.name)

```

7.(Optional) saving response and show results
```{r}
results <- getPerson(  sessionName = sess.name)
summary(results, sort =T)
```

#Conclusion 
We briefly overviewed what steps and components are required to implement mirtCAT test. For the developer of assessment template: My understanding is that the program may need to cover step 1-6 in the above section, which includes generating a subset of testset based on instructor's items choice, choosing adaptive vs non-adaptive(fixed), specifying details if adaptive test, editing interface (e.g. instructor's message), and finally saving all the configurations somewhere to be ready to deploy the specified assessment.



# 4/24 update IRT parameters
Item estimates may need to be updated once we have enough response patterns from the data collection. The issue is whether we want 
1. 'real time' update - update response dataset everytime we get new record, fit the model again, and deploy the mirtCAT test to the server
2. 'delayed' update - send a user's response to the box folder to update the response dataset that will be used to estimate the new model in fixed amount of time (manually)

Which one is for us would depend on the workload for programming. Regardless, the general iterative procedure would like be below

##Essential steps for updating

1. save a user's response to a object
```{r}
person <- getPerson(sess.name) 
summary(person, sort=FALSE) # 'sort=FALSE' gets us the full length of scored responses vector

```

2. extract a vector of scored_response
```{r}
response <- person$scored_responses
```

3. add the new response to the existing response dataset
```{r}
pdat <- rbind(pdat, response) # Here, pdat is assumed an original dataset and we just added new record
```
 
4. fit an IRT model with updated dataset to update the item parameters
```{r}
(mo_new <- mirt(data = pdat, model = 1, itemtype = 'Rasch', #16 students
                technical = list(removeEmptyRows=T)))

coef(mo_new)
```

5. Re-run mirtCAT
```{r}
sess.name = createSessionName(n = 50)

mirtCAT_preamble(
  df = testset, mo = mo_new, # <- identifying testset and model object
  AnswerFuns = answerfuns, # <- can be a list of 'NA' unless an item has specific grading rules
  criteria = 'MI', # indicates adaptive method, see'mirtCAT' help page for options
  start_item = 'MI', # decides the first item user takes, can be item number
  design = design_list, # additional setting such as maximum items to take, stopping criteria
  shinyGUI = shinyGUI_list, # specifying front page, demographics, and last page
  sessionName = sess.name # <- after version update, giving session number is a must

)
createShinyGUI(  sessionName = sess.name) # Generate shinyapp based on the above
```
NOTE: The end of Step 5 should be connected to step 1

##Thoughts
If we want 'real-time' parameter updates, final function of mirtCAT would need to include step1-4 with pdat and mo_new appropriately saved to box. Additionally, mirtCAT assessment needs to be activated by loading such files from the box folder.
In the other case, including step1-3 would be enough for final function.
In both cases, securing Box authentication may be critical for this functionality


# 4/30 providing graphical results
**I read again the developer's example code 'mirtCAT_lastpage_img.R', to figure out the image loading process on the last page after user finishes responding to assessment. Now I guess that the key part of making this happen is providing the server the location that we create and save ggplot image and, allowing for sourcing such directory in the shinyapp **

Adding image source to lastpage
```{r}
dirname <- paste0(getwd(), '/www')
dir.create(dirname)
shiny::addResourcePath('www', dirname) # I think this is the key part to load image on the display of the server

#Modify lastpage object
## remember that 'person' object is where mirtCAT saves user's responses
lastpage <- function(person){ 
    df <- data.frame(theta=person$thetas_history, id=1:nrow(person$thetas_history))
    df <- tidyr::gather(df, "factor", 'theta',  -id)
    gg <- ggplot(df, aes(id, theta)) + geom_point() + geom_line() + facet_wrap(~factor)

    filename <- 'www/lastpage.png' # 'www' folder is what we gives to the server
    ggsave(filename, plot = gg)
    list(h2("Thank you for completing the test."), 
         h4('Your final ability estimates are:'),
         h4(sprintf("Theta score: %.3f, SE = %.3f", person$thetas[1],
                    person$thetas_SE_history[nrow(person$thetas_SE_history), 1])),
         # h4(sprintf("Ability two: %.3f, SE = %.3f", person$thetas[2],
         #            person$thetas_SE_history[nrow(person$thetas_SE_history), 2])),
         img(src=filename, height="200", width="300", align = "center"),
         h3("Please close the application."))
}

# modify the shinyGUI_list
shinyGUI_list <- list(title = title, authors = authors, 
                      demographics = demographics,
                      demographics_inputIDs = c("id"), 
                      firstpage = firstpage, lastpage = lastpage)
```

test session
```{r}
sess.name = createSessionName(n = 50)

mirtCAT_preamble(
  df = testset, mo = mod4.2,
  AnswerFuns = answerfuns,
  sessionName = sess.name, 
  criteria = 'MI',start_item = 'MI',
  shinyGUI = shinyGUI_list, design = design_list
)
createShinyGUI(sessionName = sess.name)
```


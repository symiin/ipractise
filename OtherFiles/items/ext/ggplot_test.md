Given data.frame **midwest**, the code for creating a scatterplot of
**area** (x axis) vs. **poptotal** (y axis) is:

<pre><code>ggplot(midwest, aes(x=area, y=poptotal)) + <br>
  geom_point()
</code></pre>

<img src="ggplot_test_files/figure-markdown_strict/scatter-1.png" width="60%" />

You need to differentiate the points by the variable **state** with
different colors.

<img src="ggplot_test_files/figure-markdown_strict/scatter2-1.png" width="60%" />

<pre><code>ggplot(midwest, aes(x=area, y=poptotal)) + <br>
  geom_point(<span style="color:red">___</span>(col=state))
</code></pre>

Indicate the function would fill in the blank space above. [Click
here](https://www.rdocumentation.org "ggplot2") to see the reference.

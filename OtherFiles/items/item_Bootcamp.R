## Number of questions
nquest <- 30 #specify to set the number of items

# setup#
questions <- vector("character", nquest)

options <- matrix("", nrow=nquest, ncol=5) #If you want to give more than 5 options, change the ncol
answers <- vector("character", nquest) 
answerfuns <- as.list(rep(NA, nquest)) #for testthat type question
qtype <- vector("character", nquest)
stems <-  rep(NA, nquest)


# Q1: Multiple choice with external md
stems[1] <- "stems/visual_001_ggplot_geomPoint_01.md"
options[1,] <- c('title', 'xlab', 'aes', NA, NA)
qtype[1] <- "radio"
answers[1] <- 'aes'

# Q2: Chcekbox item
questions[2] <- 'Check every verb that you think is related to data wrangling'
options[2,] <- c('mutate','select', 'filter', 'logical', 'rename')
answers[2] <- 'mutate;select;filter;rename'
qtype[2] <- 'checkbox' # for multiple choices question, question type is radio


# Q3: Multiple choices with a html script
questions[3] <- 'Suppose that you run the code below:
<p>
<pre><code>a <- 3
#a <- a+2
b <- a+3
#b <- 2*b
b
</code></pre>
</p>
Which one would be the result of running the code?'
 # Note the in-text html code to make the chunk

options[3,] <- c("16",
                 "6",
                 "10",
                 "12",
                 NA) #Here, the remaining option should have 'NA" value
answers[3] <- "6"
qtype[3] <- 'radio'

#Q4: semantic - pipeline
stems[4] <- "stems/Wrangling_Semantic_004.md"
options[4,] <- c('Option A is correct',
                 'Option B is correct',
                 'Neither is good for getting such result',
                 'Both options are correct and identical in terms of result',
                 NA)
qtype[4] <- "radio"
answers[4] <- 'Both options are correct and identical in terms of result'



#q5 - data semantic1
stems[5] <- "stems/Wrangling_Semantic_001.md"
options[5,] <- c('Dataset contains values such as numbers and strings',
                 'Each column indicates variable and row indicates observation',
                 'A value belongs to a variable but not an observation',
                 'A variable contains all values measuring an attribute across subjects',
                 'An observation contains all values measured on the same subject across attributes')
qtype[5] <- "radio"
answers[5] <- 'A value belongs to a variable but not an observation'


#q6 - data semeantic2
stems[6] <- 'stems/Wrangling_Semantic_002.md'
options[6,] <- c('A',
                 'B',
                 'C',
                 'D',
                 NA)
answers[6] <- 'D'
qtype[6] <- 'radio' 


#q7 - data semeantic3
stems[7] <- 'stems/Wrangling_Semantic_003.md'
options[7,] <- c('Arrange',
                 'Spread',
                 'Separate',
                 'Unite',
                 NA)
answers[7] <- 'Arrange'
qtype[7] <- 'radio' 


#q8 - gather
stems[8] <- "stems/Wrangling_tidyr_001.md"
options[8,] <- c('df %>% gather(key=time, value=wellbeing, sub_w1, sub_w2, sub_w3)',
                 'df %>% separate(time, into=c("measure", "time"), sep = "_")',
                 'df %>% spread(key=measure, value=value)',
                 NA, NA)
qtype[8] <- "radio"
answers[8] <- 'df %>% gather(key=time, value=wellbeing, sub_w1, sub_w2, sub_w3)'

#q9 - spread
stems[9] <- "stems/Wrangling_tidyr_002.md"
options[9,] <- c('Spread',
                 'Split',
                 'Separate',
                 'Dissemble', NA)
qtype[9] <- "radio"
answers[9] <- 'Separate'

#q10 - unite
stems[10] <- "stems/Wrangling_tidyr_003.md"
options[10,] <- c('Entangle',
                 'Unite',
                 'Paste',
                 'Cbind', NA)
qtype[10] <- "radio"
answers[10] <- 'Unite'

#Mutate
# Q11
questions[11] <- 'Suppose that you run the code below:
<p>
<pre><code>
db <- data.frame(x = 1)
db %>% 
mutate(y = 1, y = y + 1, y = y + 1)
</code></pre>
</p>
What values of x and y would be produced by running the code?'
 # Note the in-text html code to make the chunk

options[11,] <- c("x =  1, y = 1",
                 "x = 1, y = 2",
                 "x = 1, y = 3",
                 "Error: value of y not found",
                 NA) #Here, the remaining option should have 'NA" value
answers[11] <- "x = 1, y = 3"
qtype[11] <- 'radio'

# Q12
questions[12] <- 'When would you use an R markdown file (.Rmd) versus a script file (.R) to save your work?
'
options[12,] <- c("If I want the relative position of the file retained (so that it is easier to load files from the same directory), I will use an .Rmd file, otherwise I will use a .R file.",
                  "When I want a complete documentation of my work in a report I will use a .Rmd. I will use a .R file for debugging and sourcing functions.",
                  "There is no significant difference between the two formats, and they can be used for the same things interchangably.",
                  "There is no benefit to using .R script files, the .Rmd format is always superior.",
                  NA) #Here, the remaining option should have 'NA" value
answers[12] <- "When I want a complete documentation of my work in a report I will use a .Rmd. I will use a .R file for debugging and sourcing functions."
qtype[12] <- 'radio'

# Q13
questions[13] <- 'Consider the four segments of code below:
<p>
<pre><code>
# Segment 1:
new_data <- read.csv("myfilename.csv")

# Segment 2:
new_data %>% 
group_by(some_cool_suff) %>% 
summarize(average = mean(avg_me, na.rm = T)) -> updated_df

# Segment 3:
avg_var <- mean(new_data$avg_me[!is.na(some_cool_stuff)], na.rm = T)

# Segment 4:
data.frame(a = 1:10, b = letters[1:10]) %>% sample_n(3)
</code></pre>
</p>
Which segments would you classify as tidyverse syntax?
(Note: tidyverse syntax = code which uses functions from the tidyverse packages for which they are no equivalent functions from Base R or other packages)'
# Note the in-text html code to make the chunk

options[13,] <- c("Segment 1 and segment 3",
                  "Segment 2 and segment 4",
                  "Segment 4",
                  "Segment 2",
                  NA) #Here, the remaining option should have 'NA" value
answers[13] <- "Segment 2"
qtype[13] <- 'radio'

# Q14
questions[14] <- "What is the purpose of the '...' argument in calling the <strong>gather</strong>?"

options[14,] <- c("Specify which variables to gather by.",
                  "Specify which variables not to gather by (using the “-” sign).",
                  "Both (a) and (b) are true",
                  "To append new variables into data",
                  NA) #Here, the remaining option should have 'NA" value
answers[14] <- "Both (a) and (b) are true"
qtype[14] <- 'radio'

# Q15
questions[15] <- "Which of the following statements is <strong>FALSE</strong>?"

options[15,] <- c("The dplyr package was developed by Hadley Wickham of RStudio",
                  "The dplyr package is an optimized and distilled version of the plyr package",
                  "The dplyr package provides new functionality and reduced computational time compared to the plyr package",
                  "None of the above",
                  "All of the above")
answers[15] <- "Both (a) and (b) are true"
qtype[15] <- 'radio'

# Q16
questions[16] <- 'Suppose the data set <strong>survey</strong> has been loaded as a tibble. 
The data set consists of the variables:
Timestamp = Time stamp,
R_exp = Level of R experience,
Banjo = Self-rated banjo,
Psych_age_yrs = Psychological age (in years),
Sleep_hrs = Hours of sleep the user got last night,
Fav_day = Favoriate day of the week,
Crisis = Encountered significant crisis recently
Consider the code below:
<p>
<pre><code>
survey %>% group_by(id) %>% __________(vars(Psych_age_yrs, Sleep_hrs, Banjo), funs(m=mean, sd=sd))
</code></pre>
</p>
What function should you fill in above to applying the functions in `funs` to the variables in `vars()`?'
# Note the in-text html code to make the chunk

options[16,] <- c("summarize",
                  "transmute",
                  "mutate",
                  "summarize_at",
                  "mutate_at")
answers[16] <- "summarize_at"
qtype[16] <- 'radio'

#q17 - filter
stems[17] <- "stems/Wrangling_dplyr_001.md"
options[17,] <- c("(i) 'select', (ii) '==', (iii) '|'",
                  "(i) 'select', (ii) '=', (iii) '|'",
                  "(i) 'filter', (ii) '==', (iii) '|'",
                  "(i) 'filter', (ii) '=', (iii) '|'",
                  "(i) 'filter', (ii) '==', (iii) '&'")
qtype[17] <- "radio"
answers[17] <- "(i) 'filter', (ii) '==', (iii) '&'"

# Q18
questions[18] <- 'Consider the code below:
<p>
<pre><code>
data %>%
filter(variable == "value") %>%
summarize(Total = sum(variable)) %>%
arrange(desc(Total))
</code></pre>
</p>
What is the correct order to apply the functions 
filter, summarize, and arrange in the spaces in (i) -- (iii) to obtain output 
that is identical to the above?
<p>
<pre><code>
data %>%
__(i)__ (__(ii)__(__(iii)__(data, variable == numeric_value), Total = sum(variable)), desc(Total))
</code></pre>
</p>'
# Note the in-text html code to make the chunk

options[18,] <- c("(i) filter, (ii) summarize, (iii) arrange",
                  "(i) arrange, (ii) summarize, (iii) filter",
                  "(i) summarize, (ii) filter, (iii) arrange",
                  "The order doesn't matter",
                  NA)
answers[18] <- "(i) arrange, (ii) summarize, (iii) filter"
qtype[18] <- 'radio'

# Q19 - starts_with
questions[19] <- 'Consider the code below, again with the data <strong>univbct</strong> already loaded:
<p>
<pre><code>
colnames(univbct)
test <- univbct %>% dplyr::select(-starts_with("JOBSAT"))
</code></pre>
</p>
With output:
> colnames(univbct)
 [1] "BTN"     "COMPANY" "MARITAL" "GENDER"  "HOWLONG" "RANK"    "EDUCATE" "AGE"    
[9] "JOBSAT1" "COMMIT1" "READY1"  "JOBSAT2" "COMMIT2" "READY2"  "JOBSAT3" "COMMIT3"
[17] "READY3"  "TIME"    "JSAT"    "COMMIT"  "READY"   "SUBNUM" 
What variables are contained in the data frame <strong>test</strong>?'
# Note the in-text html code to make the chunk

options[19,] <- c("All variables that start with JOBSAT",
                  "All variables except those that start with JOBSAT",
                  "All variables that appear after JOBSAT1, the 9th variable",
                  "It does not contain any variable as there is no variable by that name",
                  NA)
answers[19] <- "All variables except those that start with JOBSAT"
qtype[19] <- 'radio'

#q20 - gather, separate, spread
stems[20] <- "stems/Wrangling_dplyr_002.md"
options[20,] <- c("Select",
                  "Gather",
                  "Separate",
                  "Spread",
                  "Mutate")
qtype[20] <- "radio"
answers[20] <- "Gather"

#q21 - gather, separate, spread
stems[21] <- "stems/Wrangling_dplyr_003.md"
options[21,] <- c("Select",
                  "Gather",
                  "Separate",
                  "Spread",
                  "Mutate. You are trying to trick us.")
qtype[21] <- "radio"
answers[21] <- "Separate"

#q22 - Facet Wrapping in ggplot2

questions[22] <- 'Given a scatterplot created by ggplot2 with <strong>a</strong> on the x-axis and <strong>b</strong> on the y-axis from a data.frame, <strong>D</strong>,
how do you create subplots as grouped by the categorical variable, <strong>group</strong>, using the function facet_wrap()?
You can visit the <a href="https://www.rdocumentation.org" target="_blank"> help page. </a>'
qtype[22] <- "radio"
options[22,] <- c('facet_wrap(group ~)', 'facet_wrap(group ~ a)',
                    'facet_wrap(~ group)', 'facet_wrap(a ~ group)', NA)
answers[22] <- 'facet_wrap(~ group)'

#q23 - Jittering
questions[23] <- 'The following code creates a scatterplot:
<p>
<pre><code>g <- ggplot(data = D, aes(x=v1, y=v2))
g + geom_point()</code></pre>
</p>
The plot however contains many of over-plotting points because the points have the same values for <strong>v1</strong> and <strong>v2</strong>.
Which of followings stir the points to display the overlapped points? You can visit the <a href="https://www.rdocumentation.org" target="_blank"> help page</a>.'
qtype[23] <- "radio"
options[23,] <- c('g + position_nudge( )', 'g + position_dodge( )',
                  'g + scale_alpha_ordinal( )', 'g + geom_jitter( )', NA)
answers[23] <- 'g + geom_jitter( )'

#q24 - geom_bar()

questions[24] <- 'You created a ggplot object:
<p>
<pre><code>g <- Dataset %>% ggplot()</code></pre>
</p>
Which of followings creates a bar chart that shows the counts of each category of a categorical variable <strong>c1</strong>?
You can visit the <a href="https://www.rdocumentation.org" target="_blank"> help page. </a>'
qtype[24] <- "radio"
options[24,] <- c('g + geom_bar(aes(c1))', 'g + geom_rect(aes(c1))',
                     'g + geom_col(aes(c1))', 'g + geom_count(aes(c1))', NA)
answers[24] <- 'g + geom_bar(aes(c1))'

#q25 - geom_hist(bins=x)

questions[25] <- 'You created a ggplot object:
<p>
<pre><code>g <- Dataset %>% 
ggplot() + 
aes(x = x) + 
geom_histogram(binwidth = 5)</code></pre>
</p>
What does the argument <i>binwidth = 5</i> result in?' 
qtype[25] <- "radio"
options[25,] <- c('Results in 5-histogram bars', 'Results in bars that span 5-units',
                     'Both are true', 'Both are false', NA)
answers[25] <- 'Results in bars that span 5-units'

#q26 - Tricky question; missing geom_point()

questions[26] <- 'You created the following ggplot object. <strong>X</strong> and <strong>Y</strong> are continuous variables drawn from <strong>Dataset</strong>:
<p>
<pre><code>g <- Dataset %>% 
ggplot() + 
aes(x = X, y = Y)</code></pre>
</p>
What does the resulting plot look like?' 
qtype[26] <- "radio"
options[26,] <- c('A scatter plot with X on the x-axis and Y on the y-axis', 
                     'A blank canvas with axis labels for X on the x-axis and Y on the y-axis',
                     'No output is generated', NA, NA)
answers[26] <- 'A blank canvas with axis labels for X on the x-axis and Y on the y-axis'

#q27 - Purpose of geom_point(); resolution to previous question

questions[27] <- 'You created the following ggplot object. <strong>X</strong> and <strong>Y</strong> are continuous variables drawn from <strong>Dataset</strong>:
<p>
<pre><code>g <- Dataset %>% 
ggplot() + 
aes(x = X, y = Y) + 
geom_point() </code></pre>
</p>
What does the resulting plot look like?' 
qtype[27] <- "radio"
options[27,] <- c('A scatter plot with X on the x-axis and Y on the y-axis', 
                     'A blank canvas with axis labels for X on the x-axis and Y on the y-axis',
                     'No output is generated', NA, NA)
answers[27] <- 'A scatter plot with X on the x-axis and Y on the y-axis'

#q28 - Facet Wrap and Facet Grid
questions[28] <- 'What is a key difference delineating facet_wrap() from facet_grid() in the following example where <strong>X</strong> and <strong>Y</strong> are continuous variables and <strong>c1</strong> and <strong>c2</strong> are categorical variables:
<p>
<pre><code>g <- Dataset %>% 
ggplot() + 
aes(x = X, y = Y) + 
geom_point() +
faced_wrap(~c1)</code></pre>
</p>
<p>
<pre><code>g <- Dataset %>% 
ggplot() + 
aes(x = X, y = Y) + 
geom_point() +
faced_grid(c1~c2)</code></pre>
</p>' 
qtype[28] <- "radio"
options[28,] <- c('facet_wrap() is more ideal when displaying the relationship between two variables across the levels of 2-categorical variables', 
                     'facet_grid() is more ideal when displaying the relationship between two variables across the levels of 2-categorical variables',
                     'Both generate identical output', 
                     NA, NA)
answers[28] <- 'facet_grid() is more ideal when displaying the relationship between two variables across the levels of 2-categorical variables'

#q29 - ggsave() comes after ggplot()
questions[29] <- 'The following function is run:
<p>
<pre><code>ggsave("ggplot.png", width = 9, height = 9)</code></pre>
</p>
Then, this ggplot is created:
<p>
<pre><code>g <- Dataset %>% 
ggplot() + 
aes(x = X) + 
geom_histogram()</code></pre>
</p>
What is the outcome of these inputs?'
qtype[29] <- "radio"
options[29,] <- c('A .PNG file is saved containing a histogram of variable X', 
                  'Nothing is generated because ggsave() requires graphics.off() to be run after the plot code',
                  'Nothing is generated because ggsave() requires plot code be run beforehand', 
                  NA, NA)
answers[29] <- 'Nothing is generated because ggsave() requires plot code be run beforehand'

#q30 - alpha
questions[30] <- 'The following code creates a scatterplot:
<p>
  <pre><code>
    ggplot(data = D, aes(x=v1, y=v2)) + geom_point()
  </code></pre>
</p>
You can use the aesthetic <strong>alpha</strong> to make the points transparent.
Which of followings creates a scatterplot with 50% transparent points?
You can visit the <a href="https://www.rdocumentation.org" target="_blank"> help page.</a>'
qtype[30] <- "radio"
options[30,] <- c("ggplot(data = D, aes(x=v1, y=v2, alpha = 0.5)) + geom_point()",
                 "ggplot(data = D, aes(x=v1, y=v2)) + geom_point(alpha = 0.5)",
                 "ggplot(data = D, aes(x=v1, y=v2), alpha = 0.5) + geom_point()",
                 "ggplot(data = D, aes(x=v1, y=v2)) + geom_point(aes(alpha = 0.5))", 
                 NA)
answers[30] <- "ggplot(data = D, aes(x=v1, y=v2)) + geom_point(alpha = 0.5)"
#q31 - 
#q32 - 
#q33 - 
#q34 - 
#q35 - 

##----Creating dataset by combining all above-------------##
df <- data.frame(Question=questions, Option=options, Answers=answers,
                 Type = qtype, 
                 Stem = stems,
                 stringsAsFactors=FALSE)


##---deploy mirtCAT---##
# library(car)
# data(cars)  # Currently, we need to load all the relevant packages and data used in the questions before deploying mirtCAT
# library(testthat); library(ggplot2)
# 
# library(mirtCAT)
# mirtCAT_preamble(df=df, AnswerFuns=answerfuns)
# # Note that custom test function(answerfuns) is specified as an argument for mirtCAT function, not as being in the data.frame
# createShinyGUI() #creat shiny app
# person <- getPerson() #collecting the response
# str(person) # see '$scored_responses' to check if all is graded correctly
# # 

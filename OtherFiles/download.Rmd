---
title: iPRACTISE
runtime: shiny
---

```{r setup, echo=FALSE, message=FALSE}
knitr::opts_chunk$set(echo = FALSE)
library(mirtCAT)
```

```{r design, eval=FALSE}
design_list <- list(
constraints=list(excluded=c(1))
)
```

```{r mirtUI, eval=TRUE}
# change aesthetics of GUI, including title, authors, header, and initial message
title <- 'Title'
authors <- 'Authors'
    
firstpage <- list(h5('sdfg'))
    
lastpage <- function(person) {
  list(h5('gfs'))
}
    
demographics <- list(
  textInput(inputId = 'id', label = 'PSU ID', value = ''))
    
shinyGUI_list <- list(
  title = title, authors = authors, 
  demographics = demographics,
  demographics_inputIDs = c('id'), 
  firstpage = firstpage, lastpage = lastpage)
```


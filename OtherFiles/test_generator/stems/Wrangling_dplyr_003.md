Consider the screen shot below.
<br>
<img src="Wrangling_dplyr_001_files/figure-markdown_strict/dplyr_001.png" height="20%"
width=70%/>

The screenshot shows selected properties of the data set, <strong>univbct</strong>, previously published by Bliese and Ployhart (2002). Data were collected at three time points. Here we have a data frame with 19 columns from 495 individuals.
Working with variables that were measured repeatedly, including JOBSAT1-3, COMMIT1-3,
and READY1-3, a friend shows you the following code.
<p>
<pre><code>
test2 <- univbct %>% dplyr::select(SUBNUM, JOBSAT1, JOBSAT2, JOBSAT3, 
                                    COMMIT1, COMMIT2, COMMIT3, 
                                    READY1, READY2, READY3) %>% 
  gather(key="key", value="value", -SUBNUM) %>% 
  separate(col="key", into=c("variable", "occasion"), -1) %>%
  spread(key=variable, value=value) %>% mutate(occasion=as.numeric(occasion))
</code></pre>
</p>
The friend says that she accidentally commented out a portion of the code so the code stopped running before it got through the last pipe operator. This is the output from running her previous code.

<img src="Wrangling_dplyr_003_files/figure-markdown_strict/dplyr_003.png" height="20%" width=50% />

What was the last data wrangling function she ran?

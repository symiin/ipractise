Q6 [out of 30]. Which one of the following is not necessary to define **tidy data** ?

<table>
<thead>
<tr>
<th style="text-align:left;"> Option </th>
<th style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
<th style="text-align:left;"> Description </th>
</tr>
</thead>
<tbody>
<tr>
<td style="text-align:left;"> A </td>
<td style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td style="text-align:left;"> Each variable forms a column </td>
</tr>
<tr>
<td style="text-align:left;"> B </td>
<td style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td style="text-align:left;"> Each observation forms a row </td>
</tr>
<tr>
<td style="text-align:left;"> C </td>
<td style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td style="text-align:left;"> Each type of observational unit (e.g., persons, schools) forms a table </td>
</tr>
<tr>
<td style="text-align:left;"> D </td>
<td style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td style="text-align:left;"> Any observation having missing values is removed from a table </td>
</tr>
</tbody>
</table>

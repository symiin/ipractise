---
#output: html_document
output: md_document
editor_options: 
  chunk_output_type: console
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
library(knitr)
library(tidyr)
library(dplyr)
library(Hmisc)
library(readr)
library(gridExtra)
df <- data.frame(subid=1:10, 
                 sub_w1=rnorm(10, 5, 1), sub_w2=rnorm(10, 6, 1), sub_w3=rnorm(10, 7, 1),
                 anx_w1=rnorm(10, 9, 1), anx_w2=rnorm(10, 6, 1), anx_w3=rnorm(10, 7, 1))
df_long <- df %>% gather(key=time, value=value, -subid)

```

Suppose that you have data structured like below:
```{r, out.height='20%'}
grid.table(df_long[c(1:3,58:60),])
```

while the value names _sub_ indicates wellbeing, _anx_ for anxiety, and the number indicates time points being measured. You notice that the time variable has both information about the measure (sub versus anx) and time (1-3).

Which of the following functions is going to help break the time variable into two variables, as illustrated below?
```{r, out.height='20%'}
df_long2 <- df_long %>% separate(time, into=c("measure", "time"), sep = "_")
df_long2 <- df_long2 %>% mutate(time=parse_number(time))
grid.table(df_long2[c(1:3,58:60),])
```






 




Given data.frame **midwest**, the code for creating a scatterplot of
**area** (x axis) vs. **poptotal** (y axis) is:

<pre><code>ggplot(midwest, aes(x=area, y=poptotal)) + <br>
  geom_point()
</code></pre>
<img src="visual_003_ggplot_geomPoint_03_files/figure-markdown_strict/scatter-1.png" width="60%" />

You need to differentiate the points by the variable **state** with
different colors.

<img src="visual_003_ggplot_geomPoint_03_files/figure-markdown_strict/scatter2-1.png" width="60%" />

<pre><code>ggplot(midwest, aes(x=area, y=poptotal)) + <br>
  geom_point(<span style="color:red">______</span>)
</code></pre>
Fill in the blank space above. [Click
here](https://ggplot2.tidyverse.org/reference/ "ggplot2") to see the
ggplot2 reference.

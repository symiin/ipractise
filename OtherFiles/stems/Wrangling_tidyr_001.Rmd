---
#output: html_document
output: md_document
editor_options: 
  chunk_output_type: console
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
library(knitr)
library(tidyr)
library(dplyr)
library(Hmisc)
library(readr)
library(gridExtra)
df <- data.frame(subid=1:10, 
                 sub_w1=rnorm(10, 5, 1), sub_w2=rnorm(10, 6, 1), sub_w3=rnorm(10, 7, 1),
                 anx_w1=rnorm(10, 9, 1), anx_w2=rnorm(10, 6, 1), anx_w3=rnorm(10, 7, 1))
```


```{r, out.height='20%'}
grid.table(round(df[1:3,],3))
```

The data above is not especially tidy. We have three columns that represent the same variable on three occasions. It would be cleaner to have a time variable (key) and two variables representing well-being and anxiety, like an example below:

```{r, out.height='20%'}
df_long <- df %>% gather(key=time, value=wellbeing, sub_w1, sub_w2, sub_w3)
df_long <- df_long %>% mutate(time=parse_number(time))
grid.table(round(df_long[c(1:3, 11:13,21:23),], 3))
```

Which of the following functions is going to help tidy data in such way?





 




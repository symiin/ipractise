Q4 [out of 20]. Which one of the following verbs is not matched with the correct description?

<table>
<thead>
<tr>
<th style="text-align:left;"> Verbs </th>
<th style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
<th style="text-align:left;"> Description </th>
</tr>
</thead>
<tbody>
<tr>
<td style="text-align:left;"> Arrange </td>
<td style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td style="text-align:left;"> Combine multiple columns into a single column </td>
</tr>
<tr>
<td style="text-align:left;"> Spread </td>
<td style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td style="text-align:left;"> Split a single variable into multiple variables </td>
</tr>
<tr>
<td style="text-align:left;"> Separate </td>
<td style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td style="text-align:left;"> Divide key-value rows into columns </td>
</tr>
<tr>
<td style="text-align:left;"> Unite </td>
<td style="text-align:left;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td style="text-align:left;"> Merge two columns into one </td>
</tr>
</tbody>
</table>

Given data.frame **midwest**, the code for creating a scatterplot of
**area** (x axis) vs. **poptotal** (y axis), differentiated by the
variable **state** with different colors is:

<pre><code>ggplot(midwest, aes(x=area, y=poptotal)) + <br>
  geom_point(aes(colour=state))</code></pre>
<img src="visual_004_ggplot_geomPoint_04_files/figure-markdown_strict/scatter-1.png" width="60%" />

You would like to add loess line more by the variable **popdensity**
with different sizes.

<img src="visual_004_ggplot_geomPoint_04_files/figure-markdown_strict/unnamed-chunk-1-1.png" width="60%" />

<pre><code>ggplot(midwest, aes(x=area, y=poptotal)) + <br>
  geom_point(aes(color=state, <span style="color:red">___</span>=popdensity))
</code></pre>
What would fill in the blank space above? [Click
here](https://ggplot2.tidyverse.org/reference/ "ggplot2") to see the
ggplot2 reference.

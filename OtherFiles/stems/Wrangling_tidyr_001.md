Q8 [out of 30]. 
<br>
<img src="Wrangling_tidyr_001_files/figure-markdown_strict/unnamed-chunk-1-1.png" height="20%" />

In the data above, we have three columns that
represent the same variable on three occasions. A researcher would like to
have a time variable (key) and a single column of a *well-being* variable that includes 
assessments from all three occasions, as shown in the screenshot below:

<img src="Wrangling_tidyr_001_files/figure-markdown_strict/unnamed-chunk-2-1.png" height="20%" />

Which of the following functions is going to help tidy data in this way?

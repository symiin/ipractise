library(shiny)
library(shinyjs)
library(shinyBS)
library(collapsibleTree)
library(shinyWidgets)
library(boxr)
library(devtools)


row <- function(...) {
  tags$div(class="row", ...)
}

col <- function(width, ...) {
  tags$div(class=paste0("span", width), ...)
}

#box_auth(cache=".boxr-oauth", write.Renv=FALSE)
ui <- fluidPage(

  # titlePanel(div(img(src = 'https://bitbucket.org/symiin/ipractise/raw/7fd77799dd6fc5e5ea86a372b62fdebbf82d51e9/www/ipractise_logo.png',
  #                    width =160, height=45,style="float:right"), 
  #                "Material Recommendation")),

  
  tags$style(HTML(
    '.popover-title{
    color: black;
    background-color: lightblue 
    }'
  )),
  tags$style(".progress-bar {background-color: #00cccc;}"),
  ####change the location of progress bar
  tags$style(
    HTML('
        #sidebar {
         background-color: #dec4de;
         }
         
         body { 
         color: grey;
         }',
         
        ".shiny-notification {
        height:100%;
        width: 100%;
        position:fixed;
        top: calc(50% - 500px);;
        left: calc(50% - 750px);;
        }
        "
    )
    ),
#useShinyjs(),
shinyjs::useShinyjs(),
circleButton("toggleSidebar", icon = icon("toggle-on"), status = "default",size = "sm"),

sidebarLayout(
  
  div(id="Sidebar",sidebarPanel(
    tags$style(".well {background-color:#060E58;}"),
    tags$style(" #load, show{
               color:#ffffff;
              }
               "),
    uiOutput("load"),
    uiOutput("show"),
    width =4,
    
    
    bsButton(inputId = "continue", label="Load!",disabled = FALSE, type ="action")#style = "color: #fff; background-color: #337ab7; border-color: #2e6da4")
    
  )),
  mainPanel(
    
    collapsibleTreeOutput("plot"),
    
    verbatimTextOutput("materials.view"),
    uiOutput("materials.view.files.buttons"),
    div(style="display: inline-block;vertical-align:top; width: 425px;",uiOutput('websitelink')),
    div(style="display: inline-block;vertical-align:top; width: 425px;",plotOutput('radarPlot'))

  ))
  )
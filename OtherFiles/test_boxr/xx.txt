@misc{ Annenberg,
    author = "The Annenberg Foundation",
    title = "Annenberg Learner",
    year = "2016",
    url = "https://learner.org/resources/browse.html?d=5&g=5",
    note = "[Online; accessed 20-October-2017]"
  }

@misc{ Vimeo,
    author = "Vimeo Inc.",
    title = "Vimeo",
    year = "2017",
    url = "https://vimeo.com/",
    note = "[Online; accessed 20-October-2017]"
  }
  
@misc{ Khan,
    author = "Khan Academy",
    title = "Khan Academy",
    year = "2017",
    url = "https://www.khanacademy.org/",
    note = "[Online; accessed 20-October-2017]"
  }

@misc{ OpenEd,
    author = "OpenEd, a division of ACT",
    title = "OpenEd",
    year = "2017",
    url = "https://www.opened.com/search",
    note = "[Online; accessed 20-October-2017]"
  }  
---
title: "iPRACTISE"
resource_files:
- hierarchyPlot/server.R
- hierarchyPlot/ui.R
- generator/server.R
- generator/ui.R
- .boxr-oauth
- .Renviron
- items/ext/ggplot_test_files/figure-markdown_strict/scatter-1.png
- items/ext/ggplot_test_files/figure-markdown_strict/scatter2-1.png
- hierarchyPlot/.Renviron
- hierarchyPlot/.boxr-oauth
runtime: shiny
output: flexdashboard::flex_dashboard
---

```{r setup, echo=FALSE, message=FALSE}
knitr::opts_chunk$set(echo = FALSE)
library(mirtCAT)
library(data.tree)
library(collapsibleTree)
library(tidyr)
library(dplyr)
library(readr)
library(multilevel)
library(colorspace)
library(boxr)
library(shinyBS)
library(shinydashboard)

box_auth(cache="./.boxr-oauth", write.Renv=T)

# use shinyjs in rmarkdown document
library(shinyjs)
useShinyjs(rmd = TRUE)

# packages for mirtCAT
library(ggplot2)
library(testthat)
#Specifying a test item#
library(car) ## Should be deployed first before the question

```

```{r question}
# source item files
# source("items/items_visual_ggplot.R")
# source("items/item_Bootcamp.R")
#source("items/item_Bootcamp_Numbered.R")
# source("items/Bootcamp_Set1.R")
# source("items/Bootcamp_Set2.R")
 # source("items/Bootcamp_Set3.R")
```


```{r design, eval=TRUE}
# The 'design' argument of mirtCAT
# See mirtCAT help page.
design_list <- list(max_items = 5, min_SEM = 0.9)
```

```{r mirtUI, eval=TRUE}
# mirtCAT arguments related to UI
# See mirtCAT help page.
# change aesthetics of GUI, including title, authors, header, and initial message
title <- "iPRACTISE Test Script"
authors <- "Anno Zhang"
firstpage <- list(
   h4("mirtCAT Testing ")
  )

lastpage <- function(person){
    h4("Test Completed")
}

shinyGUI_list <- list(title = title, authors = authors,firstpage = firstpage, lastpage = lastpage)

```

<!--
Topic
=====================================
-->
```{r}
# navbarPage
# https://www.rdocumentation.org/packages/shiny/versions/1.2.0/topics/navbarPage
navbarPage("", id="tabs",
  tabPanel("Uploading",
    sidebarLayout(
      sidebarPanel(
        tags$h4("Test script for uploading function")
      ),
      mainPanel(

        bsButton("start","START")
      )
    )
  )


)
```

```{r server}
observeEvent(input$start, {
  
  appendTab(
    inputId="tabs",
    tabPanel("Questions",
      select = TRUE,
      shinyAppDir("./mirtCATtest",
                  options = list(width = "100%", height = 700)) )
    )
  updateTabItems(session, inputId = "tabs", selected = "Questions")

})
# mirtCAT_preamble(sessionName = sess.name,
#         # the name must matched to the name of the data.frame containing the items
#         df = df,
#         AnswerFuns=answerfuns,
#       	shinyGUI = shinyGUI_list,
#        final_fun=final_fun
#         # if you want to check the result locally
#         #final_fun=final_fun_local
#         )
```
